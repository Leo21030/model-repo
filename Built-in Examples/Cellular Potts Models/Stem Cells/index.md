---
MorpheusModelID: M0026
aliases: [/examples/stem-cells-in-the-intestinal-crypt/]
menu:
  Built-in Examples:
    parent: Cellular Potts Models
    weight: 60
weight: 160

title: "Stem Cells in the Intestinal Crypt"

authors: []
contributors: []

# Reference details
publication:
  doi: ""
  title: ""
  journal: ""
  volume: 
  page: 
  year: 
  original_model: true

#categories:
#- DOI:

tags:
- Asymmetric Cell Division
- Asymmetric Division
- CellDivision
- Cell Type
- ChangeCellType
- Clonal Population
- Domain
- Global
- Intestinal Crypt
- Logger
- Mapper
- Proliferation
- Stem Cell
- TA Cell
- Transit Amplifying Cell
- Triggers
- Rules
- Variable
---
> Demonstrates the Morpheus features asymmetric cell division (<span title="//CellTypes/CellType/CellDivision">`CellDivision`</span>), conditional change of cell type (<span title="//CellTypes/CellType/ChangeCellType/Condition">`ChangeCellType`</span>) and loading a simulation domain from an <span title="//Space/Lattice/Domain/Image/@path">`Image`</span>.

{{% callout note %}}
This model also requires the separate file [`crypt.tiff`](#downloads).
{{% /callout %}}

## Introduction

This illustrative example shows the emergence of clonal populations from stem cells in an intestinal crypt. Stem cells in the bottom of the crypt divide asymmetrically and produce a population of transit amplifying (TA) cells. For each TA cell, the color indicates the stem cell from which it was derived.

![](crypt.png "Asymmetric cell division and changing cell type.")

## Description

The model shows several new modeling features, available as of [Morpheus 1.2](/download/latest).

### Loading Domain from Image

The crypt-like domain is specified by loading an external 8-bit TIFF image file [`crypt.tiff`](#downloads) using the <span title="//Space/Lattice/Domain/Image/@path">`Image`</span> feature.

### Asymmetric Cell Division

Stem cells divide asymmetrically using the new <span title="//CellTypes/CellType/CellDivision/@daughterID">`daughterID`</span> handles in the <span title="//CellTypes/CellType/CellDivision">`CellDivision`</span> element. This sets a user-defined symbol (here called <span title="//CellTypes/CellType/CellDivision/@daughterID = 'daughter'">`daughter`</span>) to either $1$ or $2$. This symbol can then be used to distinguish both daughter cells and treat them differently. In this example, it is used to set the stemness ($s$) of one <span title="//CellTypes/CellType/CellDivision/@daughterID = 'daughter'">`daughter`</span> to $1$ and the stemness of the other daughter cell to $0$.

### Conditionally Changing Cell Types

When a cell looses its stemness $s$, it is moved to the TA cell type. This is done using the <span title="//CellTypes/CellType/ChangeCellType">`ChangeCellType`</span> element. 

Upon satisfying its <span title="//CellTypes/CellType/ChangeCellType/Condition">`Condition`</span>, <span title="//CellTypes/CellType/ChangeCellType">`ChangeCellType`</span> moves the cell to the specified new cell type. By default, all the properties of a cell that exist in both cell type contexts are maintained, and unspecified ones are set to their default values. This default behavior can be overridden using <span title="//CellTypes/CellType/ChangeCellType/Triggers">`Triggers`</span> that specify <span title="//CellTypes/CellType[@name='TA_cells']/CellDivision/Triggers/Rule">`Rules`</span> stating how to deal with specific properties.

### Mapper

<span title="//CellTypes/CellType[@name='TA_cells']/Mapper">`Mappers`</span> allow the collection of statistical data about the cell population. Here, they are used to count the sizes of the various clonal populations. This number is reported into global <span title="//Global/Variable">`Variables`</span> and subsequently written to a file and plotted using a <span title="//Analysis/Logger">`Logger`</span>.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/111262100?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
