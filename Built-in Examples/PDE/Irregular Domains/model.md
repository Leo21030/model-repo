{{% callout note %}}
This model requires the external file [`M0013_model_domain.tiff`](/media/model/m0013/M0013_model_domain.tiff), which must be inside the same directory as {{< model_quick_access "media/model/m0013/ActivatorInhibitor_Domain.xml" >}}.
{{% /callout %}}

![](M0013_model_graph.svg "Model Graph")