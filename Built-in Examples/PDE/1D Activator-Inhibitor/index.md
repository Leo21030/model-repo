---
MorpheusModelID: M0011

authors: [A. Gierer, H. Meinhardt]

title: "1D Reaction-Diffusion: Activator-Inhibitor"
date: "2019-11-05T13:39:00+01:00"
lastmod: "2020-10-30T12:29:00+01:00"

aliases: [/examples/1d-reaction-diffusion-activator-inhibitor/]

menu:
  Built-in Examples:
    parent: PDE
    weight: 10
weight: 60
---

## Introduction

The first example models a 1D activator-inhibitor model ([Gierer and Meinhardt, 1972][gierer-1972]). 

![](1d-pde.png "Space-time plot of 1D reaction diffusion model.")

## Description

This 1D PDE model uses a ```Lattice``` with ```linear``` structure and periodic boundary conditions. 

The ```PDE``` defined two species called ```Layers```: $A$ (activator) and $I$ (inhibitor) with resp. low and high ```Diffusion``` rates. The reaction part of the equations are defined in the ```System```. Similar to ODE model, the ```System``` defines ```Constants``` and ```DiffEqn```s.

The results are recorded and visualized using the ```Logger``` and ```SpaceTimeLogger```. 

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/70815567?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Reference

A. Gierer, H. Meinhardt: [A Theory of Biological Pattern Formation.][gierer-1972] *Kybernetik* **12**: 30-39, 1972.

[gierer-1972]: http://www.eb.tuebingen.mpg.de/fileadmin/uploads/pdf/Emeriti/Hans_Meinhardt/kyb.pdf