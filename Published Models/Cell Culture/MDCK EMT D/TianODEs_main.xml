<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour

Produced SI Figure  13
        
ODE model for the Tian et al model for epithelial-mesenchymal transition (EMT)
governed by TGF-beta.

Tian, X.J., Zhang, H. and Xing, J. (2013)
Coupled reversible and irreversible bistable switches
underlying TGFβ-induced epithelial to mesenchymal transition.
Biophysical journal, 105(4), pp.1079-1089.

This file was constructed by Leah Edelstein-Keshet using
an XPP .ode file kindly supplied by Prof. Xiaojun Tian.
</Details>
        <Title>TianODEs</Title>
    </Description>
    <Time>
        <StartTime value="0"/>
        <StopTime value="500" symbol="stoptime"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="1"/>
    </Time>
    <Space>
        <Lattice class="linear">
            <Size value="1, 0, 0" symbol="size"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Global>
        <Variable name="TGF" value="0" symbol="TGF"/>
        <Variable name="snail" value="0.01" symbol="snail"/>
        <Variable name="SNAIL" value="0.01" symbol="SNAIL"/>
        <Variable name="miR34" value="0.4" symbol="miR34"/>
        <Variable name="zeb" value="0.01" symbol="zeb"/>
        <Variable name="ZEB" value="0.01" symbol="ZEB"/>
        <Variable name="miR200" value="0.4" symbol="miR200"/>
        <Variable name="E-cadherin" value="4" symbol="Ecadherin"/>
        <Variable name="N-cadherin" value="0" symbol="Ncadherin"/>
        <System solver="Runge-Kutta [fixed, O(4)]" time-step="0.1">
            <Constant name="TGF0" value="3" symbol="TGF0"/>
            <Constant name="k0TGF" value="0.06" symbol="k0TGF"/>
            <Constant name="kTGF" value="1.2" symbol="kTGF"/>
            <Constant name="JTGF" value="0.06" symbol="JTGF"/>
            <Constant name="kdTGF" value="0.6" symbol="kdTGF"/>
            <Constant name="Jsnail" value="1.6" symbol="Jsnail"/>
            <Constant name="kdsnail" value="0.09" symbol="kdsnail"/>
            <Constant name="k0snail" value="0.0006" symbol="k0snail"/>
            <Constant name="ksnail" value="0.03" symbol="ksnail"/>
            <Constant name="kSNAIL" value="17.0" symbol="kSNAIL"/>
            <Constant name="JSNAIL" value="0.08" symbol="JSNAIL"/>
            <Constant name="kdSNAIL" value="1.66" symbol="kdSNAIL"/>
            <Constant value="0.2" symbol="Jsnailcad1"/>
            <Constant name="k034" value="0.0012" symbol="k034"/>
            <Constant name="k34" value="0.012" symbol="k34"/>
            <Constant name="J134" value="0.15" symbol="J134"/>
            <Constant name="J234" value="0.36" symbol="J234"/>
            <Constant name="kd34" value="0.035" symbol="kd34"/>
            <Constant name="k0zeb" value="0.003" symbol="k0zeb"/>
            <Constant name="kzeb" value="0.06" symbol="kzeb"/>
            <Constant name="Jzeb" value="3.5" symbol="Jzeb"/>
            <Constant name="kdzeb" value="0.09" symbol="kdzeb"/>
            <Constant name="kZEB" value="17" symbol="kZEB"/>
            <Constant name="JZEB" value="0.06" symbol="JZEB"/>
            <Constant name="kdZEB" value="1.66" symbol="kdZEB"/>
            <Constant name="k02" value="0.0002" symbol="k02"/>
            <Constant name="k2" value="0.012" symbol="k2"/>
            <Constant name="kd2" value="0.035" symbol="kd2"/>
            <Constant name="J12" value="5.0" symbol="J12"/>
            <Constant name="J22" value="0.2" symbol="J22"/>
            <Constant value="0.1" symbol="kecad1"/>
            <Constant value="0.06" symbol="kecad2"/>
            <Constant value="0.2" symbol="Jecad1"/>
            <Constant value="0.5" symbol="Jecad2"/>
            <Constant value="0.05" symbol="kdecad"/>
            <Constant value="0.1" symbol="kncad1"/>
            <Constant value="0.06" symbol="kncad2"/>
            <Constant value="0.2" symbol="Jncad1"/>
            <Constant value="0.5" symbol="Jncad2"/>
            <Constant value="0.05" symbol="kdncad"/>
            <DiffEqn symbol-ref="TGF" name="Equation for TGF">
                <Expression>k0TGF+kTGF/(1+(miR200/JTGF)^2)-kdTGF*TGF</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="snail" name="Equation for snail">
                <Expression>k0snail+ksnail*(((TGF+TGF0)/Jsnail)^2)/(1+((TGF+TGF0)/Jsnail)^2)-kdsnail*snail</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="SNAIL" name="Equation for SNAIL">
                <Expression>kSNAIL*snail/(1+(miR34/JSNAIL)^2)-kdSNAIL*SNAIL</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="miR34" name="Equation for miR34">
                <Expression>k034+k34/(1+(SNAIL/J134)^2+(ZEB/J234)^2)-kd34*miR34</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="zeb" name="Equation for zeb">
                <Expression>k0zeb+kzeb*(SNAIL/Jzeb)^2/(1+(SNAIL/Jzeb)^2)-kdzeb*zeb</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="ZEB" name="Equation for ZEB">
                <Expression>kZEB*zeb/(1+(miR200/JZEB)^2)-kdZEB*ZEB</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="miR200" name="Equation for miR200">
                <Expression>k02+k2/(1+(SNAIL/J12)^2+(ZEB/J22)^2)-kd2*miR200</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="Ecadherin" name="Equation for E-cadherin">
                <Expression>kecad1/((SNAIL/Jecad1)^2+1)+kecad2/((ZEB/Jecad2)^2+1)-kdecad*Ecadherin</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="Ncadherin" name="Equation for N_cadherin">
                <Expression>kncad1*((SNAIL/Jncad1)^2)/((SNAIL/Jncad1)^2+1)+kncad2*((ZEB/Jncad2)^2)/((ZEB/Jncad2)^2+1)-kdncad*Ncadherin</Expression>
            </DiffEqn>
        </System>
    </Global>
    <Analysis>
        <Logger time-step="0.1">
            <Plots>
                <Plot title="EMT Model" time-step="-1">
                    <Style decorate="true" style="lines" line-width="2.0" grid="true"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="time"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="4">
                        <Symbol symbol-ref="TGF"/>
                        <!--    <Disabled>
        <Symbol symbol-ref="miR200"/>
    </Disabled>
-->
                        <!--    <Disabled>
        <Symbol symbol-ref="miR34"/>
    </Disabled>
-->
                        <Symbol symbol-ref="SNAIL"/>
                        <Symbol symbol-ref="snail"/>
                        <Symbol symbol-ref="ZEB"/>
                        <Symbol symbol-ref="zeb"/>
                        <Symbol symbol-ref="Ecadherin"/>
                        <Symbol symbol-ref="Ncadherin"/>
                    </Y-axis>
                </Plot>
            </Plots>
            <Output>
                <TextOutput/>
            </Output>
            <Input/>
        </Logger>
        <ModelGraph include-tags="#untagged" format="dot" reduced="false"/>
    </Analysis>
</MorpheusModel>
