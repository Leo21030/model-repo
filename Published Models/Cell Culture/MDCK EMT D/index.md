---
MorpheusModelID: M7124

title: "MDCK EMT D"

authors: [N. Mukhtar, E. N. Cytrynbaum, L. Edelstein-Keshet]
contributors: [E. N. Cytrynbaum, Y. Xiao]

# Under review
#hidden: true
#private: true

# Reference details
publication:
  doi: "10.1016/j.bpj.2022.04.010"
  title: "A multiscale computational model of YAP signaling in epithelial fingering behavior"
  journal: "Biophys. J."
  volume: 121
  issue: 10
  page: 1940-1948
  year: 2022
  original_model: true

categories:
- DOI:10.1016/j.bpj.2022.04.010

tags:
- 2D
- Adherent Slow Cells
- Cancer Metastasis
- Cell-Cell Adhesion
- Cell Migration
- Cellular Potts Model
- Cell Speed
- Collective Cell Migration
- CPM
- DirectedMotion
- E-cadherin
- Embryonic Development
- EM State
- EMT
- Epithelial Cell
- Epithelial Fingering
- Epithelial/Mesenchymal State
- Epithelial-Mesenchymal Transition
- Epithelial Sheet
- Epithelium
- Finger-like Projection
- GTPase
- Intracellular Signaling
- LengthConstraint
- Mechanochemical Control
- Mesenchymal Cell
- Mesenchyme
- miRNA34
- Motile Loose Cell
- Multiscale Model
- N-cadherin
- ODE
- Ordinary Differential Equation
- Rac1
- SNAI1
- Snail
- TGFβ
- Topographic Cues
- Transforming Growth Factor
- Wound Healing
- YAP
- YAP1
- YAP65
- Yes-associated Protein
- ZEB
- ZEB1
- ZEB2
- Zinc Finger E-box-binding Homeobox 1
- Zinc Finger E-box-binding Homeobox 2
- Zinc Finger Protein SNAI1
---
> Multiscale model of epithelial-mesenchymal transition (EMT) with intracellular TGFβ signaling circuit 

## Introduction

The growth factor TGFβ regulates the snail-zeb intracellular circuit that affects EMT by controlling the levels of E and N-cadherin expression. One popular model for this circuit, due to [Tian *et al.* (2013)][Tian2013] is shown on this page. TGFβ upregulates SNAIL1 that promotes ZEB, while depressing itself through miRNA34. SNAIL1 and ZEB respectively promote the expression of E-cadherin and N-cadherin.

## Description

We first constructed an exact replica of this full [Tian *et al.* (2013)][Tian2013] ODE model in Morpheus, then ran simulations for various levels of exogenous TGFβ, the growth factor that stimulates the EMT process. As shown in the following figure, results are concordant with [Figure 3A](https://pubmed.ncbi.nlm.nih.gov/23972859/#&gid=article-figures&pid=figure-3-uid-2) of [Tian *et al.* (2013)][Tian2013]. A normal epithelial state exists, in the absence of exogenous TGFβ. This state consists of high E-cad and low levels of all other effectors. Increasing the exogenous TGFβ to $`1.5`$ results in an EM state that has moderate levels of both E- and N-cadherins. A further increase to $`3`$ leads to the ‘fully mesenchymal’ state where N-cadherin dominates and E-cadherin has been reduced to very low levels.

![](S13.png "A TGFβ EMT signaling circuit: A schematic diagram of the circuit as implemented by [Tian *et al.* (2013)](https://pubmed.ncbi.nlm.nih.gov/23972859/) (left). Setting $`\mathrm{TGF}_0 = 3`$, leads to a mesenchymal state with very low E-cad and high N-cad (right). Produced by Morpheus file [`TianODEs_main.xml`](#downloads) with equations and parameter values kindly supplied by Xiao-Jun Tian.")

In the second stage, the TGFβ intracellular circuit was inserted in our wound-healing cell sheet simulation in place of the YAP circuit. The following minimal modifications were made to adjust the multiscale model to the new circuit:

1. High N-cad cells were assumed to be ‘more mesenchymal’.
2. Cell-cell adhesion was assumed to depend only on the level of E-cadherin, as before.
3. A static gradient of exogenous TGFβ was simulated. The value of $`\mathrm{TGF}_ 0 = \mathrm{TGFβ} (x, y)`$ was assumed to directly feed into the level of $`\mathrm{TGF}_ 0`$ in any cell at the given position $`(x, y)`$ in the 2D domain. Mesenchymal cells were assumed to move faster and have longer shapes. To Implement this, we assumed that <span title="CellTypes/CellType[name=&quot;dividingcell&quot;]/DirectedMotion[strength] = 1+Ncadherin/3">`DirectedMotion[strength]`$`\ = 1 + \frac{1}{3} N_\mathrm{cad}\vec{u}`$</span>, where $`\vec{u}`$ is an unit vector in the $`x`$ direction. A <span title="CellTypes/CellType[name=&quot;dividingcell&quot;]/LengthConstraint[target, strength] = 4+4*Ncadherin, 2">`LengthConstraint`</span> is also implemented in the Hamiltonian, changing the cells to slightly longer oval shapes rather than polygonal morphology at low N-cadherin. 

![](S14.png "β cell sheet morphology: Epithelial, mesenchymal, and intermediate states in the TGFβ model by [Tian *et al.* (2013)](https://pubmed.ncbi.nlm.nih.gov/23972859/). Shown is a static exogenous TGF gradient (orange background, $`\mathrm{TGF}_0`$ in model) and cell sheet morphology for parameter values that produced the ODE simulations. Top: E-cadherin level. Bottom: N-cadherin level. As cells migrate into the high TGFβ level, their E-cadherin decreases, so cells break away and become a swarm of mesenchymal cells. Where TGFβ is low (left end of domain), E-cadherin is high enough that all cells are epithelial. An intermediate EM state is seen at moderate levels of TGFβ, in the model of the domain. Morpheus file: [`TianTGFbetaCellSheet.xml`](#downloads).")

## Results

At high TGFβ, N-cadherin dominates, and the cells are all mesenchymal. A transition zone with partial EM state is also seen in the domain, where both E- and N-cadherins are at moderate levels. Collective, rather than single cell migration is seen in that zone. 

![](TianTGFbetaCellSheet.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m7124/TianTGFbetaCellSheet.xml" >}}:** Animated results from the multiscale model derived from the TGFβ EMT signaling circuit.

[Tian2013]: https://pubmed.ncbi.nlm.nih.gov/23972859/

