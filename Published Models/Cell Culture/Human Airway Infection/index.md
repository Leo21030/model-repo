---
MorpheusModelID: M6296

title: "Human Airway Infection"
#linktitle: ""
subtitle: Viral Spread within the Human Airway Epithelium (HAE)

authors: [B. Raach, N. Bundgaard, M. J. Haase, J. Starruß, R. Sotillo, M. L. Stanifer, F. Graw]
contributors: [P. Lukas, F. Graw]
submitters: [P. Lukas, F. Graw]
curators: [D. Jahn]

# Under review
hidden: false
private: false

# Reference details
publication:
  doi: "https://doi.org/10.1371/journal.pcbi.1011356"
  title: "Influence of cell type specific infectivity and tissue composition on SARS-CoV-2 infection dynamics within human airway epitheliumn"
  journal: "PLoS Comput. Biol."
  volume: 19
  issue: 8
  page: "e1011356"
  year: 2023
  original_model: true
  preprint: false

tags:
- 2D
- Air-Liquid Interface Culture
- ALI
- ALI Culture
- Airway
- Airway Infection
- Basal Cell
- Bronchial Cell
- Bronchial Epithelial Cell
- Bronchial Epithelium
- Cell Death
- Cell Differentiation
- Cell Heterogeneity
- Cell Loss
- Cell Type Specific Infectivity
- Cell Type Specific Replication Kinetics
- Cellular Turnover
- Ciliated Cell
- Differentiation
- Disease Progression
- Epithelial Barrier
- Epithelial Regeneration
- Epithelium
- Global Spread
- HAE
- HAE Cell Culture
- HAE Culture
- HAE culture System
- Heterogeneity
- Heterogeneous Infection Pattern
- Homeostasis
- Homeostatic Condition
- Human Airway
- Human Airway Epitheliumn
- Human Airway Epithelial Culture
- Human Airway Epithelial Culture Systems
- Human Airway Infection
- Immunity
- Individual-Based Model
- Individual Cell-Based Model
- Infected Cell
- Infection
- Infection Dynamics
- Infection Kinetics
- Infection Pattern
- Infectious Cell
- Infectivity
- Linear Differentiation
- Linear Differentiation Process
- Local Spread
- Loss Rate
- Pathology
- Probabilistic Framework
- Probabilistic Model
- Proliferation
- Pseudostratified HAE
- Regeneration
- Regeneration Capacity
- Regeneration Dynamics
- Regenerative Turnover
- Replication Kinetics
- Respiratory Epithelium
- Respiratory Infection
- Respiratory Tree
- RNA
- RNA Copy
- SARS-CoV-2
- SARS-CoV-2 Infection
- SARS-CoV-2 Infection Dynamics
- Secretory Cell
- Spatial Distribution
- Susceptible Cell
- Tissue Composition
- Tissue Differentiation
- Tissue Integrity
- Tissue Regeneration
- Uninfected Cell
- Upper Epithelial Barrier
- Upper Respiratory Epithelium
- Upper Respiratory Tract
- Viral Dynamics
- Viral Infection
- Viral Load
- Viral Spread
- Viral Transmission
- Viral Transmission Mechanism
- Virion
- Virus

#categories:
#- DOI:10.1371/journal.pcbi.1011356
---
> Viral Spread within the Human Airway Epithelium (HAE)

[deborjacallejas2014]: https://doi.org/10.1371/journal.pone.0100537
[schamberger2015]: https://doi.org/10.1038/srep08163

## Introduction

The spread of viral infections within tissues depends on several factors including the spatial distribution and turnover dynamics of target cells. In [Raach _et al._ (2023)](#reference), a Cellular Potts model was developed to investigate the influence of target cell heterogeneity within the human airway epithelium on SARS-CoV-2 infection dynamics. The study aims to understand how different tissue compositions and regenerative turnover throughout the respiratory tree affect the progression of SARS-CoV-2 infections. The model incorporates cell-type specific infection kinetics, tissue regeneration, and accounts for both global and local spread of infections through cell-free and cell-associated viral transmission mechanisms.

Parameterization of the model is achieved by integrating published experimental data on differentiating primary human bronchial epithelial cells within air-liquid interface cultures through approximate Bayesian computation ([de Borja Callejas _et al._, 2014][deborjacallejas2014], [Schamberger _et al._, 2015][schamberger2015]). By simulating SARS-CoV-2 infections given various tissue compositions and regenerative capacities, the model provides insights into cell-type specific infection dynamics and the impact of tissue composition on disease progression and pathology.

## Description

The human airway epithelium (HAE) cell culture system is represented by a 2D individual cell-based model within a radial lattice grid. The simulation initializes a total population of $`\approx 4.7 \times 10^4`$ cells, with each cell occupying a target area of 60 lattice sites. The model considers three different cell types: basal (B), secretory (S), and ciliated (C) cells, which constitute the epithelium with different relative contributions. To mimic the pseudostratified 3D-morphology of the upper respiratory epithelium, the volume strength of basal cells is set to $`\frac{1}{10}`$ of that of secretory or ciliated cells, allowing basal cells to be ‘overgrown’ by the other cell types, i.e., being potentially compressed to only one lattice site in fully differentiated tissue. 

Cellular turnover and epithelial regeneration follows a linear differentiation process with continuous proliferation of basal cells at a density-dependent rate <span style="white-space: nowrap;">($`\alpha`$)</span>, subsequent differentiation into secretory and ciliated cells ($`\lambda_\mathrm{S}`$, $`\lambda_\mathrm{C}`$) and cell loss according to cell-type specific death rates ($`\delta_\mathrm{B}`$, $`\delta_\mathrm{S}`$, $`\delta_\mathrm{C}`$) (see also [Fig. 1](#fig-1)). The processes of cell proliferation, differentiation, and cell death are incorporated into the Morpheus model by translating the model system shown in [Fig. 1](#fig-1) into a probabilistic framework (see also [Raach _et al._, 2023](#reference)).

<div id="fig-1">
![](M6296_human-airway-infection_fig-1_cell-differentiation-dynamics.png "<strong>Figure 1. A:</strong> Sketch of the linear differentiation process and cellular turnover of basal (B), secretory (S) and ciliated (C) cells within the epithelium under homeostatic conditions. Basal cells proliferate at a density-dependent rate and subsequently differentiate into secretory and ciliated cells with rate $`\lambda_\mathrm{S}`$ and $`\lambda_\mathrm{C}`$, respectively. Cell types are lost according to their individual loss rates ($`\delta_\mathrm{B}`$, $`\delta_\mathrm{S}`$, $`\delta_\mathrm{C}`$). <strong>B:</strong> Snapshots of the Cellular Potts model at various time points depicting the differentiation and regeneration of the human airway epithelium to mimic the pseudo-stratified epithelium of the upper respiratory tract. (Figures partly reproduced and adapted from [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/): [**Raach _et al._, 2023**](#reference), [Fig. 1B, D](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1011356#pcbi-1011356-g001).)")
</div>

To incorporate viral infection, the model follows the standard model of viral dynamics: After infection of a cell, the cell progresses through an eclipse phase before it becomes infectious and starts to produce new virions. Each cell type can get infected through either cell-free or cell-associated transmission modes dependent on the viral concentration or the number of infectious cells in its neighborhood, respectively, as well as the cell-type specific transmission rates, $`\beta_\mathrm{cf}`$ and $`\beta_\mathrm{cc}`$ (see also [Raach et al., 2023](#reference) for further details).

The model code {{< model_quick_access "media/model/m6296/M6296_human-airway-infection_model.xml" >}} provided here simulates viral infection within bronchial epithelium given an initial cell type distribution of $`39.7\ \text{\%}`$ (B), $`13.4\ \text{\%}`$ (S), and $`46.9\ \text{\%}`$ (C), at equilibrium before infection. A simulation time step represents $`0.25`$&nbsp;hours in real time. Infection is initialized by adding a viral load of $`2.6 \times 10^6`$ RNA copies at $`6.25`$&nbsp;hours ($`= 25`$&nbsp;time steps) post simulation start, with the epithelium being at confluence. For all individual parameters, see also [Raach _et al._, 2023](#reference). 


## Results

[Vid. 1](#vid-1) and [Fig. 2](#fig-2) show the results of re-running the model with the parametrizations and initial cell distributions as outlined in [Raach _et al._, 2023](#reference) over a time period of $`7`$ days.

<figure id="vid-1">
  <div style="margin-bottom: -1.5rem;">
    ![](M6296_human-airway-infection_vid-1_simulation.mp4)
  </div>
  <figcaption>
    <strong>Video 1.</strong> Visualization of the dynamics of the infection process in a single simulation, showing the destruction of the upper epithelial barrier by the infection. Susceptible cells: green (basal), blue (secretory) and orange (ciliated). Infected cells: brown (basal), purple (secretory) and red (ciliated).
  </figcaption>
</figure>

Results reproduce the findings reported by [Raach _et al._](#reference) with ciliated cells showing the highest fraction of infected cells ([Fig. 2B](#fig-2)). Snapshots of the Cellular Potts model ([Fig. 2D](#fig-2)) indicate the effect of heterogeneous infection patterns on tissue integrity (see also [Raach _et al._, 2023](#reference)).

<div id="fig-2">
![](M6296_human-airway-infection_fig-2_infection-kinetics.png "<strong>Figure 2. A-C:</strong> Simulated kinetics of <strong>(A)</strong> the total cell count and <strong>(B)</strong> the fraction of infected cells for each of the different cell types, as well as <strong>(C)</strong> the viral load followed over a time course of $`7`$ days post infection. Lines and shaded area represent the mean and $`90\ \text{\%}`$ confidence band across ten repeated simulations, respectively. <strong>D:</strong> Snapshots of the Cellular Potts model at various time points of a single simulation depicting the spatial distribution of the SARS-CoV-2 infection across the human airway epithelium.")
</div>
