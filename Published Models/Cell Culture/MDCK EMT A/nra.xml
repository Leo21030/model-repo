<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>
        Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour.
        
        
This xml file was used to produce  Figure 4.

We thank Lutz Brusch for providing a basic cell sheet simulation that we modified and adapted to this project.
        
        
        </Details>
        <Title>nra</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size value="400, 100, 0" symbol="size"/>
            <BoundaryConditions>
                <Condition type="constant" boundary="x"/>
                <Condition type="noflux" boundary="-x"/>
                <Condition type="periodic" boundary="y"/>
                <Condition type="periodic" boundary="-y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="1500" symbol="stoptime"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="1"/>
    </Time>
    <Analysis>
        <Gnuplotter time-step="50" decorate="true">
            <Terminal name="png"/>
            <Plot>
                <Cells value="Y" flooding="true">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="lemonchiffon"/>
                            <Color value="0.05" color="light-blue"/>
                            <Color value="0.1" color="light-red"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <Plot title="Rac1">
                <Cells value="R" flooding="true" min="0" max="3">
                    <ColorMap>
                        <Color value="0" color="blue"/>
                        <Color value="3" color="red"/>
                        <Color value="4" color="yellow"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Plot>
                <Cells value="E" flooding="true" min="0" max="2">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="plum"/>
                            <Color value="4" color="blue"/>
                            <Color value="7" color="cyan"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <Plot>
                <Cells value="d.abs" flooding="true">
                    <!--    <Disabled>
        <ColorMap>
            <Color value="0" color="skyblue"/>
            <Color value="0.1" color="violet"/>
            <Color value="0.2" color="salmon"/>
        </ColorMap>
    </Disabled>
-->
                </Cells>
            </Plot>
            <!--    <Disabled>
        <Plot>
            <Cells value="Cr" flooding="true">
                <Disabled>
                    <ColorMap>
                        <Color value="0" color="red"/>
                        <Color value="0" color="red"/>
                    </ColorMap>
                </Disabled>
            </Cells>
        </Plot>
    </Disabled>
-->
        </Gnuplotter>
        <!--    <Disabled>
        <Logger time-step="0">
            <Input>
                <Symbol symbol-ref="cell.id"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot>
                    <Style style="points" point-size="0.5"/>
                    <Terminal terminal="png"/>
                    <X-axis minimum="0.0">
                        <Symbol symbol-ref="dist"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="25">
                        <Symbol symbol-ref="truavspeed2"/>
                    </Y-axis>
                </Plot>
            </Plots>
        </Logger>
    </Disabled>
-->
        <!--    <Disabled>
        <Logger time-step="0">
            <Input>
                <Symbol symbol-ref="cell.id"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot>
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="dist"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="1.5">
                        <Symbol symbol-ref="Y"/>
                    </Y-axis>
                </Plot>
            </Plots>
        </Logger>
    </Disabled>
-->
        <Logger time-step="1.0">
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="300">
                    <Style style="points" point-size="0.05"/>
                    <Terminal terminal="png"/>
                    <X-axis minimum="0.0" maximum="size.x">
                        <Symbol symbol-ref="cell.center.x"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="size.y">
                        <Symbol symbol-ref="cell.center.y"/>
                    </Y-axis>
                    <Color-bar minimum="0.0" maximum="0.3" palette="rainbow">
                        <Symbol symbol-ref="d.abs"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
        <!--    <Disabled>
        <Logger time-step="1.0">
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot>
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis minimum="0.0" maximum="size.x">
                        <Symbol symbol-ref="cell.center.x"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="size.y">
                        <Symbol symbol-ref="cell.center.y"/>
                    </Y-axis>
                    <Color-bar>
                        <Symbol symbol-ref="truavspeed"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
    </Disabled>
-->
        <ModelGraph reduced="false" format="dot" include-tags="#untagged"/>
    </Analysis>
    <Global>
        <Constant value="0.1" symbol="ky" name="Basal rate of YAP activation"/>
        <Constant value="1.8" symbol="kye" name="E-cadherin-dependent rate of YAP deactivation"/>
        <Constant value="2" symbol="Dy" name="Inactivation rate of YAP"/>
        <Constant value="0.9" symbol="C" name="Initial activation rate of E-cadherin"/>
        <Constant value="0.9" symbol="ke" name="YAP-dependent rate of E-cadherin expression"/>
        <Constant value="1" symbol="K" name="Dissociation constant of YAP-WT1 transcriptional constant"/>
        <Constant value="1" symbol="De" name="Inactivation rate of E-cadherin"/>
        <Constant value="3" symbol="h" name="Hill coefficient for E-cadherin"/>
        <Constant value="1" symbol="kr" name="YAP-dependent rate of Rac1 expression"/>
        <Constant value="0.5" symbol="Kr" name="Michaelis-Menten-like constant for Rac1"/>
        <Constant value="0.5" symbol="Dr" name="Degradation rate of Rac1"/>
        <Constant value="6" symbol="n" name="Hill coefficient for Rac1"/>
        <Constant value="1" symbol="alphaR" name="Rac activation fraction"/>
        <Constant value="1.8" symbol="kyr" name="Rac1-dependent rate of YAP activation"/>
        <!--    <Disabled>
        <Constant value="2" symbol="A2" name="basal adhesion">
            <Annotation>E-cad blocking</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant value="12" symbol="A2" name="Max E-cadherin adhesion constant">
            <Annotation>Control</Annotation>
        </Constant>
        <Constant value="0.85" symbol="A3" name="E-cadherin half &quot;saturation&quot; "/>
        <Constant value="0.4" symbol="C1" name="basal migration"/>
        <Constant value="4" symbol="C2" name="Max Rac1 migration constant"/>
        <Constant value="3" symbol="C3" name="Rac1 half &quot;saturation&quot;"/>
        <Constant value="100" symbol="tlim" name="max time for initial leader cells to emerge"/>
        <Constant value="0.2" symbol="frac" name="fraction of neighbourhood Cr that the cell receives each time Cr spreads to it"/>
        <!--    <Disabled>
        <Constant value="0.5" symbol="Ytot" name="Total YAP (active (Y) + inactive)">
            <Annotation>KD</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant value="1.2" symbol="Ytot" name="Total YAP (active (Y) + inactive)">
            <Annotation>Control</Annotation>
        </Constant>
        <!--    <Disabled>
        <Constant value="2" symbol="Ytot" name="Total YAP (active (Y) + inactive)">
            <Annotation>OE</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant value="5" symbol="Rtot" name="Total Rac1 (active (R) + inactive)"/>
        <Constant value="0.02" symbol="shareprob" name="parameter that determines probability of Cr spread in each time step"/>
        <Variable value="0.0" symbol="E" name="E-cadherin"/>
        <Variable value="0.0" symbol="Cr" name="Basal Rac1 activation rate"/>
        <!--    <Disabled>
        <Variable value="0.0" symbol="x_edge2"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Mapper name="leftmost edge cell" time-step="1.0">
            <Input value="(M>0)*cell.center.x*(cell.center.x>10) + (M==0)*size.x + (M>0)*size.x*(cell.center.x&lt;=10)"/>
            <Output mapping="minimum" symbol-ref="x_edge2"/>
        </Mapper>
    </Disabled>
-->
    </Global>
    <CellTypes>
        <CellType name="medium" class="medium"/>
        <CellType name="dividingcell" class="biological">
            <VolumeConstraint target="50" strength="1"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint target="1" mode="aspherity" strength="1"/>
            <CellDivision division-plane="major">
                <Condition>(rand_uni(0,1)&lt;0.006*(exp(-time/300)+0.2)) * (cell.center.x&lt;40)</Condition>
                <Triggers/>
                <Annotation>Only cells in the first 40 pixels can divide, regardless of domain size. Before, this was size.x*0.1</Annotation>
            </CellDivision>
            <CellDeath name="delete cells near right domain edge">
                <Condition>(cell.center.x>0.99*size.x)</Condition>
            </CellDeath>
            <DirectedMotion name="cell migration" direction="1, 0.0, 0.0" strength="C1+C2*R/(C3+R)"/>
            <Property value="0" symbol="Y" name="YAP"/>
            <Property value="10" symbol="E" name="E-cadherin"/>
            <Property value="0" symbol="R" name="Rac1"/>
            <Property value="0.001" symbol="Cr"/>
            <Property value="0.0" symbol="dist" name="distance from right domain edge"/>
            <Property value="0.0" symbol="avspeed" name="Sum of instantaneous speeds each time step (times 100)"/>
            <Property value="0.0" symbol="truavspeed" name="Average Speed * 100"/>
            <Property value="0.0" symbol="avspeed2" name="Sum of instantaneous speeds over final 200 time steps (times 100)"/>
            <Property value="0.0" symbol="truavspeed2" name="average speed over final 200 time steps (times 100)"/>
            <Property value="0.0" symbol="M" name="Contact with medium"/>
            <Property value="0.0" symbol="neigh" name="number of neighbour cells"/>
            <Property value="0.0" symbol="av" name="average neighbourhood Cr"/>
            <PropertyVector value="0.0, 0.0, 0.0" symbol="d" name="speed"/>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.medium.id" scaling="length"/>
                <Output mapping="sum" symbol-ref="M"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.dividingcell.id" scaling="cell"/>
                <Output mapping="sum" symbol-ref="neigh"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="Cr" scaling="cell"/>
                <Output mapping="average" symbol-ref="av"/>
            </NeighborhoodReporter>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>
            <System solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn name="Equation for YAP" symbol-ref="Y">
                    <Expression>(ky+kyr*R)*(Ytot-Y) - (kye*Y*E + Dy*Y)</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for E-cadherin" symbol-ref="E">
                    <Expression>C - ke*Y^h/(K^h+Y^h) - De*E</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for Rac1" symbol-ref="R">
                    <Expression>alphaR*(Cr + kr*Y^n/(Kr^n+Y^n))*(Rtot-R) - Dr*R</Expression>
                </DiffEqn>
                <Rule name="Equation for Cr spread; at each time step before tlim, cells in contact with the medium have a small chance of being endowed with high Cr; at each time step, each cell has a small chance of having its Cr be augmented by the average neighbourhood Cr (up to a max value); cells at the left edge of the domain retain low Cr" symbol-ref="Cr">
                    <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)+frac*av*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>20)</Expression>
                </Rule>
                <Rule name="distance from right domain edge" symbol-ref="dist">
                    <Expression>size.x - cell.center.x</Expression>
                </Rule>
                <Rule name="Sum of instantaneous speeds each time step (times 100)" symbol-ref="avspeed">
                    <Expression>avspeed+d.abs*100</Expression>
                </Rule>
                <Rule name="average speed over the simulation (times 100)" symbol-ref="truavspeed">
                    <Expression>avspeed/time</Expression>
                </Rule>
                <Rule name="Sum of instantaneous speeds over final 100 time steps (times 100)" symbol-ref="avspeed2">
                    <Expression>avspeed2 + d.abs*100*(time > stoptime-201)</Expression>
                </Rule>
                <Rule name="average speed over 100 time steps (times 100)" symbol-ref="truavspeed2">
                    <Expression>avspeed2/200</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="Cr">
            <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)+frac*0.1*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>size.x/10)</Expression>
        </Rule>
    </Disabled>
-->
            </System>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population type="dividingcell" name="Initialize cell sheet at left edge of the domain" size="1">
            <!--    <Disabled>
        <InitRectangle mode="regular" number-of-cells="70">
            <Dimensions origin="size.x/100, 0, 0" size="size.x/100, size.y, size.z"/>
        </InitRectangle>
    </Disabled>
-->
            <InitRectangle mode="regular" number-of-cells="70">
                <Dimensions origin="0.0, 0.0, 0.0" size="4.0, size.y, 0.0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <CPM>
        <Interaction>
            <Contact value="30" type1="dividingcell" type2="dividingcell">
                <AddonAdhesion adhesive="A2*E/(A3+E)" name="Adhesive strength" strength="5"/>
            </Contact>
            <Contact value="12" type1="dividingcell" type2="medium"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
</MorpheusModel>
