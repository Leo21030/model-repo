---
MorpheusModelID: M4283

title: "Self-Organization of Leaf Polarity"

authors: [E. Scacchi, G. Paszkiewicz, K. T. Nguyen, S. Meda, A. Burian, W. de Back, M. C. P. Timmermans]
contributors: [E. Scacchi]

# Under review
hidden: false
private: false

# Reference details
publication:
  doi: "https://doi.org/10.1038/s41477-024-01634-x"
  title: "A diffusible small-RNA-based Turing system dynamically coordinates organ polarity"
  journal: "Nat. Plants"
  #volume:
  #issue: 
  #page: ""
  year: 2024
  original_model: true
  preprint: false

tags:
- Ad-Ab Polarity
- Adaxial-Abaxial Polarity
- Anisotropic Growth
- Anticlinal Cell Division
- Arabidopsis
- Arabidopsis thaliana
- Bipolar Pattern
- BoundaryCondition
- Collective Behavior
- CPM
- Feedback Loop
- Feedback Regulation
- Gene Regulatory Network
- GRN
- Growing Primordium
- Hexagonal Lattice
- Leaf
- Leaf Development
- Leaf Primordium
- Linear Bipolar Pattern
- Medio-Lateral Axis
- miR166
- Mouse-Ear Cress
- Negative Feedback Loop
- noflux
- Organ
- Organ Polarity
- Patterning
- Plant
- Plant Cell
- Polarity
- Self-Organization
- Small RNA
- Synthetic Biology
- tasiARF
- Thale Cress
- Turing Dynamics
- Turing Model
- Turing Pattern
- Turing System

categories:
- DOI:10.1038/s41477-024-01634-x
---
> A diffusible small-RNA-based Turing system dynamically coordinates organ polarity.

## Introduction

The formation of a flat and thin leaf presents a developmentally challenging problem, requiring intricate regulation of top-bottom (adaxial-abaxial) polarity. The patterning principles controlling the spatial arrangement of these domains during organ growth have remained unclear. Scacchi *et al.* has shown that this regulation is achieved by an organ-autonomous Turing reaction-diffusion system centered on mobile small RNAs. They validated experimentally how a Turing dynamics transiently instructed by pre-patterned information is sufficient to self-sustain properly oriented polarity in a dynamic, growing organ.

## Description

miR166 and tasiARF (mobile small RNAs) regulate top-bottom polarity on plants by limiting expression of their respective Class III HOMEODOMAIN LEUCINE ZIPPER (HD-ZIPIII) and AUXIN RESPONSE FACTOR3/4 (ARF3/4) targets, which, in conjunction with ASYMMETRIC LEAVES1/2 (AS) and KANADI (KAN) transcription factors, distinguish top versus bottom cell fate via further antagonistic interactions ([Fig. 1](#fig-1)).

<div id="fig-1">
![](Figure_1.jpg "<strong>Figure 1.</strong> <strong>A six-component Gene Regulatory Network (GRN) propagates ad-ab leaf polarity in the growing primordium.</strong> GRN proposed to propagate ad-ab polarity in the growing primordium, with previously established direct regulatory interactions indicated. Yellow nodes, top determinants; blue nodes, bottom determinants; red edges, interactions defined a priori in the theoretical analysis; serpentine arrows, mobility; dissolving triangles, miR166 and tasiARF gradients.")
</div>

The organizing principle underlying the GRN configuration that allows for the emergence and regulation of the top-bottom polarity, however, remains elusive. Scacchi *et al.* adopted a theoretical approach that considers all possible regulatory interactions, except those for miR166 and tasiARF, which only repress their specific targets. With the understanding that top and bottom cell fates are mutually exclusive, they addressed the possibility that a Turing reaction-diffusion system governs their domain organization. By using a high-throughput theoretical approach, Scacchi *et al.* identified a single 3-component network, the Leaf Polarity Model (LPM), as the minimal topological Turing model potentially capable to self-organize organ polarity. In this model, top determinants represented by distinct <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='u']">$`\text{HDZIPIII}`$</span> and <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='z']">$`\text{AStasiARF}`$</span> nodes form a negative feedback loop, and act in mutual opposition to bottom determinants, combined into a single <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='v']">$`\text{miRKANARF}`$</span> node ([Fig. 2](#fig-2), [Equation 1](#eq-1)). Experimental validation has substantiated the architecture delineated by the LPM.

<div id="fig-2">
![](Figure_2.jpg "<strong>Figure 2.</strong> The Leaf Polarity Model (LPM) is the only 3-component network topology compatible with Turing dynamics and imposed biological constraints.")
</div>

<figure id="eq-1">

$$\left\\{
\begin{aligned}
\frac{\partial \text{HDZIPIII}}{\partial t} &= -0.5 \cdot \text{miRKANARF} + \text{AStasiARF} - \text{HDZIPIII}^3\\\\
\frac{\partial \text{miRKANARF}}{\partial t} &= -0.5 \cdot \text{HDZIPIII} - \text{miRKANARF} \pm \text{AStasiARF}\\\\
&\quad\ + D_{\text{miRKANARF}} \cdot \nabla^2 \cdot \text{miRKANARF}\\\\
\frac{\partial \text{AStasiARF}}{\partial t} &= \text{HDZIPIII}\\\\
&\quad\ + D_{\text{AStasiARF}} \cdot \nabla^2 \cdot \text{AStasiARF}
\end{aligned}
\right.
$$
  <figcaption>
      <span title="//CellTypes/CellType[@name='cells']/System"><strong>Equation 1</strong></span>
  </figcaption>
</figure>

## Results

### 1 – The LPM self-organizes a linearly-separated bipolar pattern in a 2-dimensional static grid of cells.

To address whether the LPM can coordinate the collective behaviour of cells to generate a spatial pattern at the organ level that resembles the bipolarity of a planar leaf, Scacchi *et al.* implemented a lattice of <span title="//Space/Lattice[@class='hexagonal']/Size[@value='30, 6, 0']">$`30 \times 6`$</span> hexagonal continuous static cells as a template to mimic the newly emerging leaf. To simulate inter- and intra-cellular chemical reactions at the organ level, they adopted a cellular agent-based model (ABM), where each cell elaborates the LPM and the diffusion and perception of mobile determinants occurs between contiguous cells relative to the cell-to-cell contact surface. They applied <span title="//Space/Lattice/BoundaryConditions/Condition[boundary='y' and type='noflux']">no-flux boundary conditions</span> to both the top and bottom edges of the model template. Numerical simulations using [Equation 1](#eq-1), <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='Dv' and @value='7.5']">$`D_{\text{miRKANARF}} = 7.5 `$</span>, <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='Dz' and @value='75']">$`D_{\text{AStasiARF}} = 75`$</span>, and initial random perturbations in a range of <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='v']/@value = 'rand_uni(0.1, 0)'">$`\pm 0.1`$ for $`\text{miRKANARF}`$</span> from the homogeneous steady-state <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='u' or @symbol='v' or @symbol='z']">$`(\text{HDZIPIII}_0, \text{miRKANARF}_0, \text{AStasiARF}_0) = (0, 0, 0)`$</span>. The simulations generate a spatially stable pattern with <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='u']">$`\text{HDZIPIII}`$</span> and <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='z']">$`\text{AStasiARF}`$</span> out of phase from <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='v']">$`\text{miRKANARF}`$</span>, but the pattern is variegated rather than bipolar ([Fig. 3A](#fig-3), {{< model_quick_access "media/model/m4283/Supplementary_Code_1.xml" >}}).

<figure id="fig-3">
![](Figure_3.jpg)
  <figcaption>
    <strong>Figure 3.</strong> The LPM generates a linearly-separated bipolar pattern in a static cell-based environment. (A) Numerical simulations of the LPM following random initial perturbations in the range of <span title='//CellTypes/CellType[@name=&apos;cells&apos;]/Variable[@symbol=&apos;v&apos;]/@value = &apos;rand_uni(0.1, 0)&apos;'>$`\pm 0.1`$ for $`\text{miRKANARF}`$</span> from the homogeneous steady state <span title='//CellTypes/CellType[@name=&apos;cells&apos;]/Variable[@symbol=&apos;u&apos; or @symbol=&apos;v&apos; or @symbol=&apos;z&apos;]'>$`(\text{HDZIPIII}_0, \text{miRKANARF}_0, \text{AStasiARF}_0) = (0, 0, 0)`$</span> in a grid of hexagonal static cells with <span title='//Space/Lattice/BoundaryConditions/Condition[boundary=&apos;y&apos; and type=&apos;noflux&apos;]'>no-flux boundary conditions</span> for the top and bottom edges yields a variegated Turing pattern ({{< model_quick_access "media/model/m4283/Supplementary_Code_1.xml" >}}). (B) In contrast, introducing <span title='//CellTypes/CellType[@name=&apos;cells&apos;]/Variable[@symbol=&apos;v&apos;]'>$`\text{miRKANARF}`$</span> in the bottom cell layer as the initial condition, the system self-organizes over time into a bipolar pattern reminiscent of the adaxial-abaxial domain organization in a planar leaf ({{< model_quick_access "media/model/m4283/Supplementary_Code_2.xml" >}}). Simulation time <span title='//Time/StopTime[@value=&apos;300&apos;]'>$`t = 300`$</span>. M-L, medio-lateral axis.
  </figcaption>
</figure>

Considering that the adaxial-abaxial polarity network in young primordia from existing polarity, they next ran a numerical simulation as above, but with <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='v']">$`\text{miRKANARF}`$</span> in the bottom cell layer as the initial condition from the homogeneous steady-state as:

<span title="//CellTypes/CellType[@name='cells']/Property[@symbol='v' and @value='if( cell.center.x > size.x/2 - 60 and cell.center.x < size.x/2 + 50 and cell.center.y < size.y/7, 5, 0)']">
$$
\begin{aligned}
\text{if} \left[\text{cell center on the}\ y\ \text{axis} \right] < \frac{\left[\text{size of}\ y\right]}{7}\ \text{then}\ \text{miRKANARF}_0 &= 5,\\\\
\text{otherwise}\ \text{miKANARF}_0 &= 0
\end{aligned}
$$
</span>

([Fig. 3B](#fig-3), {{< model_quick_access "media/model/m4283/Supplementary_Code_2.xml" >}}). Importantly, this ‘pre-patterned information’ is just an initial perturbation from the steady-state, and is _not_ fixed over time, but instead immediately dissolves into the LPM during simulation. With the additional input of a polarly-localized rather than a stochastic initial perturbation, the simulation produces a bipolar pattern with <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='u']">$`\text{HDZIPIII}`$</span> and <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='z']">$`\text{AStasiARF}`$</span> marking the upper and <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='v']">$`\text{miRKANARF}`$</span> the lower cell layers. Therefore, in a defined cellular-based model, the LPM coordinates the collective behaviour of cells to self-organize into a linearly-separated bipolar tissue pattern, provided it perceives an initial polarized input for one of the system components. This transient pre-patterned information, in addition to generating bipolar stripes, also orients the stripes, such that polarity network components accumulate in the appropriate cell layers.

### 2 – The LPM self-organizes a robust linear polarity boundary in a growing cellular model

To assess whether the LPM can self-organize a robust bipolar pattern also in a growing template, Scacchi *et al.* generated a Cellular Potts model (CPM), in which cells are depicted as discrete and geometrically extended objects, and the plasma membrane and cell wall are simplified as a single entity ({{< model_quick_access "media/model/m4283/Supplementary_Code_3_main.xml" >}}). While this animal-based model can yield sliding effects between cells, this potential limitation was minimized by optimizing model conditions to mimic the immobility of plant cells. Moreover the focus is on the collective cell behaviour at the organ level, occasional subtle deviations at the scale of individual cells can be tolerated. The CPM through the Morpheus platform thus struck an ideal balance between rapid, high-throughput simulations and relevant representation of the biological system. 

To reproduce the anisotropic growth of a developing leaf primordium in silico, Scacchi *et al.* implemented a constrained environment in which only anticlinal <span title="//CellTypes/CellType[@name='cells']/CellDivision">cell divisions</span> occur as <span title="//CellTypes/CellType[@name='cells']/CellDivision/Triggers/Rule[@symbol-ref='time_next_division']/Expression[@value='time + rand_uni(0,GR)']">random events</span> over time. Upon division, the content of each mother cell is equally distributed between the 2 daughter cells with a <span title="//CellTypes/CellType[@name='cells']/CellDivision/Triggers/Rule[@symbol-ref='u' or @symbol-ref='v' or @symbol-ref='z']/Expression[@value = '0.5*u + rand_norm(0, 0.001)' or @value = '0.5*v + rand_norm(0, 0.001)' or @value = '0.5*z + rand_norm(0, 0.001)']">random perturbation of $`\pm 0.001`$</span>. In addition, by setting a fixed <span title="//CellTypes/CellType[@name='cells']/VolumeConstraint">target cell size</span>, the daughter cells will expand to reach the mother cell size. In this way, cell division-driven growth is sufficient to generate a flat domain.

Following the proportions of a primordium shortly after emergence, they implemented the initial conditions as a population of <span title="//CellPopulations/Population[@type='cells']/InitCellObjects/Arrangement[@repetitions='10, 6, 1']">$`10 \times 6`$ cells</span> with <span title="//Space/Lattice/BoundaryConditions/Condition[boundary='y' and type='noflux']">no-flux boundary conditions</span> for the upper and lower edges and an initial perturbation from the homogeneous steady-state <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='u' or @symbol='v' or @symbol='z']">$`(\text{HDZIPIII}_0, \text{miRKANARF}_0, \text{AStasiARDF}_0) = (0, 0, 0)`$</span> for <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='v']">$`\text{miRKANARF}`$</span> in the abaxial most cell layer as 

<span title="//CellTypes/CellType[@name='cells']/Property[@symbol='v' and @value='if( cell.center.x > size.x/2 - 60 and cell.center.x < size.x/2 + 50 and cell.center.y < size.y/6, 5, 0)']">
$$
\begin{aligned}
\text{if} \left[\text{cell center on the}\ y\ \text{axis} \right] < \frac{\left[\text{size of}\ y\right]}{7}\ \text{then}\ \text{miRKANARF}_0 &= 5,\\\\
\text{otherwise}\ \text{miKANARF}_0 &= 0
\end{aligned}
$$
</span>

As for simulations on the static grid of cells, each cell elaborates the LPM and diffusion of determinants between cells occurs relative to the cell-to-cell contact surface. Onto this cellular template, they introduced a division rate as a scalar <span title="//CellTypes/CellType[@name='cells']/Property[@symbol='time_next_division' and @value='50 + rand_uni(50,GR)']">division time</span> to allow the template to grow. Before the simulation starts, a random number between 1 and division time is assigned to each cell. 

<figure id="vid-1">
  ![](Video_1.mp4)
  <figcaption>
    <strong>Video 1.</strong> <strong>A single three-component minimal network can self-organize a robust linear polarity boundary.</strong> The LPM dynamically adjusts the distributions of its components over time to organize and propagate a linear bipolar pattern also in a growing cellular environment phenocopying the leaf ($`t = 2`$; $`t = 4000`$). M-L, medio-lateral axis.
  </figcaption>
</figure>

When the simulation time (<span title="//CellTypes/CellType[@name='cells']/CellDivision/Condition[@value='time > time_next_division']">`time`</span>) reaches the random number assigned to the cell, this cell divides. After cell division, a <span title="//CellTypes/CellType[@name='cells']/CellDivision/Triggers/Rule[@symbol-ref='time_next_division']/Expression[@value='time + rand_uni(0,GR)']">new random number</span> between time (at the division event) and time + division time is independently assigned to each daughter cell. Accordingly, the greater the division time value, the less frequently a cell division event occurs, the slower the system grows. When the simulation time is equal to the division time + 1, all original cells will have undergone at least one division event. By simulation of the LPM ([Equation 1](#eq-1)), <span title="//CellTypes/CellType[@name='cells']/Constant[@symbol='GR' and @value='6400']">division time $`= 6400`$</span>, <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='Dv' and @value='7.5']">$`D_{\text{miRKANARF}} = 7.5 `$</span>, <span title="//CellTypes/CellType[@name='cells']/Variable[@symbol='Dz' and @value='100']">$`D_{\text{AStasiARF}} = 100`$</span> ([Fig. 4](#fig-4)), the results show that it can self-organize a robust bipolar pattern with the proper spatial distribution of components. The pattern is continuously adapted or maintained over time, as is expected for self-organizing systems in a dynamic context. Therefore, the LPM can self-organize a robust linear boundary separating adaxial and abaxial determinants in a growing cellular environment that phenocopies a mediolateral section of the leaf ([Video 1](#vid-1)). 

<div id="fig-4">
![](Figure_4.jpg "<strong>Figure 4.</strong> <strong>A single three-component minimal network can self-organize a robust linear polarity boundary.</strong> The LPM dynamically adjusts the distributions of its components over time to organize and propagate a linear bipolar pattern also in a growing cellular environment phenocopying the leaf ($`t = 2`$; $`t = 4000`$). M-L, medio-lateral axis.")
</div>

## Conclusions

The study of Scacchi *et al.* clarifies how plants organize top-bottom polarity during primordium growth, elucidating a fundamental patterning process critical to morphogenesis. Through cell-cell communication and feedback regulation, the polarity GRN functioning in each cell orchestrates a collective cell behaviour that continuously organizes the spatial arrangement of top-bottom domains at the organ level. As such, top-bottom gene expression can dynamically adapt to internal and external perturbations to sustain a robust polarity boundary in planar leaves while providing the flexibility needed to support morphological diversity. 

Their conclusions are supported by experimental validation of essential theoretical constraints, including novel network interactions, differential mobility at the scale of the small RNA morphogens and a relatively short half-life for miR166. In addition, the variegated and often counterintuitive cell fate rearrangements in top-bottom polarity mutants align with Turing dynamics. Consequently, their findings uncover that mobile small RNAs directly interacting with transcription factors can generate Turing dynamics. Given the prevalence of small RNA-transcription factor modules in development, additional examples of this type of reaction-diffusion system may follow. Moreover, the system’s simplicity, relative to receptor-ligand driven Turing networks, combined with the adaptability of small RNAs, offers exciting new perspectives in synthetic biology.