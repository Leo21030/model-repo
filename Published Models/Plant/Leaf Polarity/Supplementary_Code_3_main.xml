<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Full title:		Self-Organization of Leaf Polarity
Authors:		E. Scacchi, G. Paszkiewicz, K. T. Nguyen, S. Meda, A. Burian, W. de Back, M. C. P. Timmermans
Contributors:	E. Scacchi
Date:		26.02.2024
Software:       	Morpheus (open-source). Download from https://morpheus.gitlab.io
Model ID:		https://identifiers.org/morpheus/M4283
File type:		Main model
Reference:		This model is the original used in the publication, up to technical updates:
		E. Scacchi, G. Paszkiewicz, K. T. Nguyen, S. Meda, A. Burian, W. de Back, M. C. P. Timmermans: A diffusible small-RNA-based Turing system dynamically coordinates organ polarity. Nat. Plants, 2024.
		https://doi.org/10.1038/s41477-024-01634-x
Comment:		To test if a Turing-like system (the Leaf Polarity Model, LPM) can self-organize a stable bipolarly striped pattern also in a growing template, we generated a Cellular Potts Model (CPM), where cells are represented as discrete and geometrically extended objects.  As a simplification, the plasma membrane and cell wall were defined as a unique entity, and CPM parameters were chosen to minimize the sliding effect between cells. In the early leaf primordium, cell divisions are randomly distributed. Additionally, the plane of cell division in the primordium is mainly oriented parallel to the adaxial-abaxial axis (anticlinal), and the direction of growth is mechanically constrained such that the leaf elaborates along the mediolateral and proximodistal axes rather than the adaxial-abaxial axis. Therefore, to reproduce the anisotropic growth of a developing leaf primordium in silico, we implemented a constrained environment, in which only anticlinal cell divisions occur as random events over the time. Upon division, the content of each mother cell is equally distributed between the 2 daughter cells with a random perturbation of +/- 0.001. In addition, by setting a fixed target cell size, the daughter cells will expand to reach the mother cell size. In this way, cell division-driven growth is sufficient to generate a flat domain.
Following the proportions of a primordium shortly after emergence from the meristem, we implemented the initial conditions as a population of 6 x 10 cells with no-flux boundary conditions for the upper and lower edges and an abaxial pulse of miRKANARF expression  as a short transient perturbation from the homogeneous steady-state that dynamically resolves into the LPM during the simulation. Each cell independently elaborates the LPM (HDZIP = u,  miRKANARF = v, AStasiARF = z) and diffusion of determinants between cells occurs relative to the cell-to-cell contact surface. Onto this cellular template, we introduced a division rate as a scalar division time to allow the template to grow. Before the simulation starts, a random number between 1 and division time is assigned to each cell. When the simulation time (time) reaches the random number assigned to the cell, this cell divides. After cell division, a new random number between time (at the division event) and time + division time is independently assigned to each daughter cell. Accordingly, the greater the division time value, the less frequently a cell division event occurs, the slower the system grows. When the simulation time is equal to the division time + 1, all original cells will have undergone at least one division event. 
This model shows that a Turing-like system can self-organize and maintain the propagation of polarity during appendage development.</Details>
        <Title>M4283: Leaf Polarity Model – Growing Organ</Title>
    </Description>
    <Space>
        <Lattice class="hexagonal">
            <Neighborhood>
                <Order>4</Order>
            </Neighborhood>
            <Size symbol="size" value="500,72,0"/>
            <BoundaryConditions>
                <Condition type="periodic" boundary="x"/>
                <Condition type="noflux" boundary="y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="5000"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="biological" name="cells">
            <Property symbol="c" value="0"/>
            <Property symbol="u" value="0&#xa;"/>
            <Property symbol="v" value="if( cell.center.x > size.x/2 - 60 and &#xa;    cell.center.x &lt; size.x/2 + 50 and &#xa;    cell.center.y &lt; size.y/6, 5, 0) "/>
            <Property symbol="z" value="0.0"/>
            <Property symbol="v_n" value="0"/>
            <Property symbol="z_n" value="0"/>
            <Variable symbol="Dv" value="7.5"/>
            <Variable symbol="Dz" value="100"/>
            <Constant symbol="GR" name="Grow Rate" value="6400"/>
            <System time-step="0.05" solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn symbol-ref="u">
                    <Expression>k1*u+k2*v+k3*z -u^3</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="v">
                    <Expression>k4*u+k5*v+k6*z
+ Dv*(0.5*v_n - 0.5*v)  </Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="z">
                    <Expression>k7*u+k8*v+k9*z 
+ Dz*(0.5*z_n - 0.5*z)
</Expression>
                </DiffEqn>
                <Constant symbol="k1" value="0.0"/>
                <Constant symbol="k2" value="-0.5&#xa;"/>
                <Constant symbol="k3" value="-1"/>
                <Constant symbol="k4" value="-0.5"/>
                <Constant symbol="k5" value="-1&#xa;&#xa;&#xa;"/>
                <Constant symbol="k6" value="-1"/>
                <Constant symbol="k7" value="1"/>
                <Constant symbol="k8" value="0&#xa;&#xa;"/>
                <Constant symbol="k9" value="0&#xa;"/>
                <Rule symbol-ref="c">
                    <Expression>if( c > 0, c-1, 0)</Expression>
                </Rule>
            </System>
            <NeighborhoodReporter>
                <Input scaling="cell" value="v"/>
                <Output symbol-ref="v_n" mapping="average"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input scaling="cell" value="z"/>
                <Output symbol-ref="z_n" mapping="average"/>
            </NeighborhoodReporter>
            <VolumeConstraint target="125" strength="1"/>
            <SurfaceConstraint target="0.5" strength="1" mode="aspherity"/>
            <Property symbol="time_next_division" value="50 + rand_uni(50,GR)"/>
            <CellDeath>
                <Condition>if( cell.center.x > size.x - 20 or cell.center.x &lt; 20, 1, 0) </Condition>
            </CellDeath>
            <CellDivision orientation="1, 0.0, 0.0" division-plane="oriented">
                <Condition>time > time_next_division</Condition>
                <Triggers>
                    <Rule symbol-ref="time_next_division">
                        <Expression>time + rand_uni(0,GR)</Expression>
                    </Rule>
                    <Rule symbol-ref="u">
                        <Expression>0.5*u + rand_norm(0, 0.001)</Expression>
                    </Rule>
                    <Rule symbol-ref="v">
                        <Expression>0.5*v + rand_norm(0, 0.001)</Expression>
                    </Rule>
                    <Rule symbol-ref="z">
                        <Expression>0.5*z + rand_norm(0, 0.001)</Expression>
                    </Rule>
                    <Rule symbol-ref="c" name="color after division">
                        <Expression>1000</Expression>
                    </Rule>
                </Triggers>
            </CellDivision>
        </CellType>
        <CellType class="medium" name="medium"/>
    </CellTypes>
    <CellPopulations>
        <Population type="cells" size="0">
            <InitCellObjects mode="distance">
                <Arrangement displacements="10, 10, 1" random_displacement="2" repetitions="10, 6, 1">
                    <Sphere radius="8" center="200,5,0"/>
                </Arrangement>
            </InitCellObjects>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="50" decorate="true" file-numbering="time">
            <Terminal name="png"/>
            <Plot title="HD-ZIPIII">
                <Cells per-frame-range="true" max="0.2" min="-0.2" value="u">
                    <ColorMap>
                        <Color value="5" color="red"/>
                        <Color value="-5" color="black"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Plot title="AStasiARF">
                <Cells per-frame-range="true" max="0.02" min="-0.02" value="z">
                    <ColorMap>
                        <Color value="5" color="green"/>
                        <Color value="-5" color="black"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Plot title="miRKANARF">
                <Cells per-frame-range="true" max="0.1" min="-0.1" value="v">
                    <ColorMap>
                        <Color value="5" color="blue"/>
                        <Color value="-5" color="black"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <!--    <Disabled>
        <Plot>
            <Cells value="time_next_division">
                <ColorMap>
                    <Color value="1" color="black"/>
                    <Color value="0" color="yellow"/>
                </ColorMap>
            </Cells>
        </Plot>
    </Disabled>
-->
            <Plot title="Cell Division">
                <Cells max="1" min="0.0" value="c">
                    <ColorMap>
                        <Color value="1" color="red"/>
                        <Color value="0.0" color="white"/>
                    </ColorMap>
                </Cells>
                <!--    <Disabled>
        <CellLabels fontsize="8" value="d"/>
    </Disabled>
-->
            </Plot>
        </Gnuplotter>
        <ModelGraph format="png" reduced="false" include-tags="#untagged"/>
        <!--    <Disabled>
        <ModelGraph format="svg" reduced="false" include-tags="#untagged"/>
    </Disabled>
-->
    </Analysis>
    <Global>
        <Constant symbol="u" value="0.0"/>
        <Constant symbol="v" value="0.0"/>
        <Constant symbol="z" value="0.0"/>
        <Constant symbol="c" value="0.0"/>
    </Global>
    <CPM>
        <Interaction>
            <Contact type1="cells" type2="cells" value="1"/>
            <Contact type1="cells" type2="medium" value="20"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>4</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics yield="0.5" temperature="2"/>
            <Neighborhood>
                <Order>3</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
</MorpheusModel>
