<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Title>Planarian Regeneration</Title>
        <Details>Full title: Spatio-temporal model of planar polarity regulation during planarian regeneration

Date: November 18, 2019
Author: michael.kuecken@tu-dresden.de and lutz.brusch@tu-dresden.de
Software: Morpheus (open-source), download from https://morpheus.gitlab.io

Reference: This model is described in the peer-reviewed publication "Dynamic Polarization of the Multiciliated Planarian Epidermis between Body Plan Landmarks" by H. T.-K. Vu, S. Mansour, M. Kuecken, C. Blasse, C. Basquin, J. Azimzadeh, E. W. Myers, L. Brusch, J. C. Rink. Developmental Cell 51, 526, 2019.
https://doi.org/10.1016/j.devcel.2019.10.022

This simulation reproduces Figure 4C of the referenced paper. Parameter values correspond to the wildtype pattern (Control) as given in Table S2 of the referenced paper.

The model explores the establishment and stability of the wildtype polarity pattern. Initial conditions for A and M fields are chosen as arbitrarily perturbed to demonstrate stability of the wildtype pattern. (AX, AY) represents the A/P polarization system, (MX, MY) represents the M/L polarization system and (PX, PY) the resulting superimposed and normalized polarization. (PX, PY) is compared to the measured polarity data of ciliary rootlets.</Details>
    </Description>
    <Space>
        <Lattice class="square">
            <Size symbol="s" value="160, 50, 0"/>
            <NodeLength value="1"/>
            <BoundaryConditions>
                <Condition boundary="x" type="noflux"/>
                <Condition boundary="-x" type="noflux"/>
                <Condition boundary="y" type="noflux"/>
                <Condition boundary="-y" type="noflux"/>
            </BoundaryConditions>
            <Domain boundary-type="noflux">
                <Image path="Planarian_regeneration_domain.tif"/>
            </Domain>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="l"/>
    </Space>
    <Time>
        <StartTime value="0.0"/>
        <StopTime value="100"/>
        <TimeSymbol symbol="t"/>
    </Time>
    <Analysis>
        <Gnuplotter time-step="10">
            <Terminal persist="true" name="png" size="2048 2048 0"/>
            <Plot>
                <Field symbol-ref="AX"/>
            </Plot>
            <Plot>
                <Field symbol-ref="AY"/>
            </Plot>
            <Plot title="(A_x, A_y)">
                <VectorField coarsening="5" value="3*AX, 3*AY, 0.0" color="black" style="2"/>
            </Plot>
            <Plot>
                <Field symbol-ref="MX"/>
            </Plot>
            <Plot>
                <Field symbol-ref="MY"/>
            </Plot>
            <Plot title="(M_x, M_y)">
                <VectorField coarsening="5" value="3*MX, 3*MY, 0.0" color="black" style="2"/>
            </Plot>
            <Plot>
                <Field symbol-ref="PX"/>
            </Plot>
            <Plot>
                <Field symbol-ref="PY"/>
            </Plot>
            <Plot title="(P_x, P_y)">
                <VectorField coarsening="5" value="3*PX, 3*PY, 0.0" color="black" style="2"/>
            </Plot>
        </Gnuplotter>
        <DependencyGraph format="svg" reduced="false"/>
    </Analysis>
    <Global>
        <Field symbol="AX" name="AX" value="rand_uni(0,1)*sin(3*pi*l.x/s.x)*sin(3*pi*l.y/s.y)">
            <Diffusion rate="5"/>
        </Field>
        <Field symbol="AY" name="AY" value="rand_uni(0,1)*sin(3*pi*l.x/s.x)*cos(3*pi*l.y/s.y)">
            <Diffusion rate="5"/>
        </Field>
        <Field symbol="MX" name="MX" value="rand_uni(0,1)*sin(3*pi*l.x/s.x)*sin(3*pi*l.y/s.y)">
            <Diffusion rate="0.67"/>
        </Field>
        <Field symbol="MY" name="MY" value="rand_uni(0,1)*sin(3*pi*l.x/s.x)*cos(3*pi*l.y/s.y)">
            <Diffusion rate="0.67"/>
        </Field>
        <Field symbol="BX" value="0.0">
            <Data filename="Planarian_regeneration_boundary_normal_Bx.txt" encoding="base64"></Data>
        </Field>
        <Field symbol="BY" value="0.0">
            <Data filename="Planarian_regeneration_boundary_normal_By.txt" encoding="base64"></Data>
        </Field>
        <Field symbol="H" name="Head" value="if(l.x&lt;10,1,0)*if(l.y&lt;27,1,0)*if(l.y>22,1,0) "/>
        <Field symbol="T" name="Tail" value="if(l.x>149,1,0)*if(l.y&lt;27,1,0)*if(l.y>22,1,0) "/>
        <Field symbol="RX" value="0"/>
        <Field symbol="RY" value="0"/>
        <Field symbol="K" value="1"/>
        <Field symbol="PX" name="PX" value="0.0"/>
        <Field symbol="PY" name="PY" value="0.0"/>
        <Constant symbol="alphaT" value="4.0"/>
        <Constant symbol="alphaH" value="2.5"/>
        <Constant symbol="alphaB" value="1.0"/>
        <Constant symbol="betaT" value="1.0"/>
        <Constant symbol="betaH" value="1.0"/>
        <Constant symbol="betaB" value="1.0"/>
        <Constant symbol="gammaA" value="0.0033"/>
        <Constant symbol="gammaB" value="0.0033"/>
        <Constant symbol="deltaA" value="1.0"/>
        <Constant symbol="deltaM" value="1.0"/>
        <System name="Pattern" solver="runge-kutta" time-step="0.1">
            <DiffEqn symbol-ref="AX">
                <Expression>(alphaT*T-alphaH*H)*BX - AX*(betaT*T+betaH*H)*abs(BX) - gammaA*AX</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="AY">
                <Expression>(alphaT*T-alphaH*H)*BY - AY*(betaT*T+betaH*H)*abs(BY) - gammaA*AY</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="MX">
                <Expression>alphaB*BX - betaB*MX*abs(BX) - gammaB*MX</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="MY">
                <Expression>alphaB*BY - betaB*MY*abs(BY) - gammaB*MY</Expression>
            </DiffEqn>
            <Rule symbol-ref="RX">
                <Expression>deltaA*AX + deltaM*MX</Expression>
            </Rule>
            <Rule symbol-ref="RY">
                <Expression>deltaA*AY + deltaM*MY</Expression>
            </Rule>
            <Rule symbol-ref="K">
                <Expression>sqrt(RX*RX+RY*RY)</Expression>
            </Rule>
            <Rule symbol-ref="PX">
                <Expression>RX/(K+0.01)</Expression>
            </Rule>
            <Rule symbol-ref="PY">
                <Expression>RY/(K+0.01)</Expression>
            </Rule>
        </System>
    </Global>
</MorpheusModel>
