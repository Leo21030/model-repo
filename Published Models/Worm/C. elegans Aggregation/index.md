---
MorpheusModelID: M7683

authors: [L. Avery, B. Ingalls, C. Dumur, A. Artyukhin]
contributors: [L. Brusch]

title: "Caenorhabditis elegans L1 Aggregation"

# Reference details
publication:
  doi: "10.1371/journal.pcbi.1009231"
  title: "A Keller-Segel model for C elegans L1 aggregation"
  journal: "PLOS Computational Biology"
  volume: 17(7)
  page: "e1009231"
  year: 2021
  original_model: true
  
tags:
- 2D
- Adhesion
- Analysis
- C. elegans
- Caenorhabditis elegans
- Cell Migration
- Cellular Potts Model
- Chemotaxis
- CPM
- Function
- Keller-Segel Model
- Larva
- Larval Stage
- Long Range Repellent
- Multicellular Model
- Nematode
- Parallel CPM
- Partial Differential Equation
- PDE
- Runge-Kutta Solver
- Short-range Attractant
- Spatial Model
- Stochastic Model

categories:
- DOI:10.1371/journal.pcbi.1009231
---
## Introduction

Larvae of the nematode Caenorhabditis elegans aggregate into clusters of several hundred moving individuals when starved during their first larval stage (see video below). [Avery _et al._](#reference) proposed that starved larvae produce and respond chemotactically to two diffusible chemical signals, a short-range attractant and a longer range repellent.

![](Video_S1.mp4)
[Video S1](https://doi.org/10.1371/journal.pcbi.1009231.s008) as published in Avery _et al._, shows the aggregation dynamics of 500&thinsp;000 starved C. elegans larvae over 12 hours ([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/): [**Avery _et al._**](#reference)).

Avery _et al._ have modelled worm motility and the proposed interaction in two different mathematical frameworks,
1. continuous Keller-Segel-type partial differential equations (PDEs) of worm density with an additional repellent concentration field and
2. a discrete individual-based Cellular Potts Model (CPM) in which each worm represents a stochastically and chemotactically moving `cell` coupled to PDEs for attractant and repellent.

Here, this hybrid CPM + PDE model for wild type worms is optimized for faster execution. This optimization and the parallel implementation of the CPM solver (from [Morpheus version 2.3.0](/download/) onward) made it possible to simulate the full individual-based system of 72&thinsp;000 interacting worms for the first time. Below, published results by Avery _et al._ are reproduced and new results for the full-scale system are presented.

## Model Description

Avery _et al._ have shared their Cellular Potts Model of C. elegans L1 aggregation as Morpheus model file at [GitHub](https://github.com/leonavery/worm-CPM).
The authors' Morpheus model was inspired by a simpler Morpheus model of worm aggregation that [Leah Edelstein-Keshet](/author/l.-edelstein-keshet/) had developed, for details see second paragraph in the authors' [README file](https://github.com/leonavery/worm-CPM/blob/main/README). 
The published model was developed with [Morpheus version 2.2.1](/download/2.2.1/) and the here provided and accelerated model was run in the newer Morpheus version 2.3.0 and is being tested with each [latest version of Morpheus](/download/latest).

To accelerate model execution, the computation of observables that do not feed back on the system dynamics was separated from the frequent updates of the PDEs and moved to `Analysis`/`Function` where they get updated when needed for output. Also, CPM updates let the lattice configuration fluctuate from MCS to MCS and the source terms in the PDEs need not be updated with each fluctuation but could be kept constant over multiple MCS. Here, we've chosen to keep the source terms in the PDEs fixed for as many MCS as a worm occupies lattice nodes and chose the 4th order Runge-Kutta solver with a `time-step` of $1\ \mathrm{s}$. The CPM is updated in Monte Carlo steps of $0.15\ \mathrm{s}$. Worm target size is set to 5&nbsp;lattice&nbsp;nodes as was calculated from the size of a real worm and the chosen lattice resolution with a node length of $0.0026\ \mathrm{cm}$ on a $2\ 304$ by $2\ 304$ square lattice. Altogether, the parallel implementation of the CPM solver (from Morpheus version 2.3.0 onward) and the above optimizations allowed to run the full-scale model within 2 days of runtime using 16 parallel threads and Linux OS. This works without interruption and does no longer require checkpointing and restarts, hence we have shifted the time interval from $1\ 500$ - $201\ 500$ back to $0$ - $200\ 000$. The `time` unit is second and the `space` unit is cm. All parameter values are as provided with the original model at [GitHub](https://github.com/leonavery/worm-CPM).

Below, two optimized model files are shared:

1. {{< model_quick_access "media/model/m7683/model_main.xml" >}} has 9&thinsp;000 worms on a $1\ \mathrm{cm} \times 1\ \mathrm{cm}$ square, was derived from the authors' [`worm6c.xml`](https://github.com/leonavery/worm-CPM/blob/main/worm6/worm6c.xml) and reproduces the results as published in [Figs. S3(C), S3(D) and S3(E)](https://doi.org/10.1371/journal.pcbi.1009231.s003) as well as [Video S8](https://doi.org/10.1371/journal.pcbi.1009231.s015) in Avery _et al._. The published model required 9 days runtime (p.19/25 in Avery _et al._) while the optimized model speeds up more than 40 fold to just 5 hours.

2. {{< model_quick_access "media/model/m7683/model_full.xml" >}} has 72&thinsp;000 worms on a $6\ \mathrm{cm} \times 6\ \mathrm{cm}$ square and was derived from the authors' [`worm7a.xml`](https://github.com/leonavery/worm-CPM/blob/main/worm7/worm7a.xml). The published model was computationally infeasible to run to completion (p.19/25 in [Avery _et al._](#reference)) while the optimized model completes in 2 days on a 16 thread processor.

## Results

### Published Simulation Results for Small System

![](Fig_S3_CDE.png "Published results from [Fig. S3](https://doi.org/10.1371/journal.pcbi.1009231.s003) by [Avery _et al._](#reference): Results of model simulation for small system of 9000 worms on a $1\ \mathrm{cm} \times 1\ \mathrm{cm}$ square at time $200.000\ \mathrm{s}$ ([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/): [**Avery _et al._**](#reference))")

### Reproduced Simulation Results for Small System

![](Fig_small_system.png "Reproduced results corresponding to [Fig. S3](https://doi.org/10.1371/journal.pcbi.1009231.s003): Simulation results of the optimized [`model_main.xml`](/media/model/m7683/model_main.xml) for the small system of 9000 worms on a $1\ \mathrm{cm} \times 1\ \mathrm{cm}$ square at time $200.000\ \mathrm{s}$")

![](Video_small_system.mp4)

Video of the simulation of the optimized {{< model_quick_access "media/model/m7683/model_main.xml" >}} for the small system of 9000 worms on a $1\ \mathrm{cm} \times 1\ \mathrm{cm}$ square reproduces the dynamics of aggregate formation and fusion as published in [Video S8](https://doi.org/10.1371/journal.pcbi.1009231.s015). Numbers at bottom right give time in seconds.


### New Simulation Results for Full-scale System

![New simulation results for full-scale individual-based model support published results of continuous worm-density model in Fig. 6 and Video S6](Fig_full_system.png "The new simulation results (bottom row) for the optimized full-scale individual-based model [`model_full.xml`](/media/model/m7683/model_full.xml) support the published results (top row) of the continuous worm density model as shown in [Fig. 6](https://doi.org/10.1371/journal.pcbi.1009231.g006) of [Avery _et al._](#reference). Left column shows initial conditions, middle column the pattern at (around) $100.000\ \mathrm{s}$ and right column the final pattern at (around) $200.000\ \mathrm{s}$. The three panels in the top row were copied from the published [Video S6](https://doi.org/10.1371/journal.pcbi.1009231.s013) ([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/): [**Avery *et al.***](#reference)). The bottom panels show individual worms as black objects overlayed onto the field $V/\sigma$ according to the color bar at the bottom right.")

When 72&thinsp;000 worms are seeded in the center of a $6\ \mathrm{cm} \times 6\ \mathrm{cm}$ square in the optimized full-scale individual-based model {{< model_quick_access "media/model/m7683/model_full.xml" >}} (bottom row in above figure), then round and regularly spaced aggregates are observed in the simulations near the periphery of an expanding disc-shaped area (bottom middle and right in above figure). Individual worms are seen to explore the entire square. Average aggregate size decreases towards the periphery of the disc and aggregates form labyrinth patterns in the high-density center of the disc. These qualitative features agree well with the continuum results (top row in above figure).

![](Video_full_system.mp4)

Video of the simulation of the optimized {{< model_quick_access "media/model/m7683/model_full.xml" >}} for the full-scale system of 72&thinsp;000 worms on a $6\ \mathrm{cm} \times 6\ \mathrm{cm}$ square. The left panel (Worms, $V/\sigma$) of the first, middle and last time frame ($200\ 000\ \mathrm{s}$) of this video correspond to the bottom row panels in the above figure.
Numbers at bottom right give time in seconds.

To allow detailed inspection of worm shapes and distributions, 21 uncompressed plots of the full-scale simulation every $10\ 000\ \mathrm{s}$ are provided in the attached [`plots.zip`](/media/model/m7683/plots.zip) archive.