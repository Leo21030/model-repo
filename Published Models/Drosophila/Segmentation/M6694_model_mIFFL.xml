<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Title>Diffusion Model of Gap Genes in Drosophila melanogaster</Title>
        <Details>Full title:		The multicellular incoherent feedforward loop motif generates spatial 			patterns
Date:		16.03.2019
Authors:		M. R. Regueira, J. D. García, A. R.-P. Aradas
Contributors:	M. R. Regueira
Software:       	Morpheus (open-source). Download from https://morpheus.gitlab.io
Units:		[time] = min, [space] = μm · 0.5
Model ID:		https://identifiers.org/morpheus/M6694
Comment:		Autoregulatory multicellular incoherent feedforward loop (mIFFL) that 			generates the 7-stripe even-skipped (eve) expression pattern in Drosophila 			melanogaster embryogenesis
                    		This model is the original used in the publication, up to technical updates.
Reference:		M. R. Regueira, J. D. García, A. R.-P. Aradas: The multicellular incoherent 			feedforward loop motif generates spatial patterns. bioRxiv, 2019.
                		https://doi.org/10.1101/579342</Details>
    </Description>
    <Global>
        <System time-step="1.0" solver="Euler [fixed, O(1)]">
            <DiffEqn symbol-ref="BICOID">
                <Expression>p_bicoid-deg_bicoid*BICOID</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="M_NANOS">
                <Expression>p_nanos-deg_nanos*M_NANOS</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="HUNCHBACK">
                <Expression>p_hunchback-deg_hunchback*HUNCHBACK</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="CAUDAL">
                <Expression>p_caudal-deg_caudal*CAUDAL</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="GIANT">
                <Expression>p_giant-deg_giant*GIANT</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="KRUPPEL">
                <Expression>p_kruppel-deg_kruppel*KRUPPEL</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="KNIRPS">
                <Expression>p_knirps-deg_knirps*KNIRPS</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="TAILLESS">
                <Expression>p_tailless-deg_tailless*TAILLESS </Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="EVE_REP">
                <Expression>p_eve_rep-deg_eve_rep*EVE_REP</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="EVE_REP_CONS">
                <Expression>p_eve_rep_cons-deg_eve_rep_cons*EVE_REP_CONS</Expression>
            </DiffEqn>
            <Constant symbol="deg_bicoid" value="0.000"/>
            <Constant symbol="deg_nanos" value="0.000"/>
            <Constant symbol="deg_hunchback" value="0.146"/>
            <Constant symbol="deg_caudal" value="0.00243"/>
            <Constant symbol="deg_kruppel" value="0.067"/>
            <Constant symbol="deg_giant" value="0.087"/>
            <Constant symbol="deg_knirps" value="0.065"/>
            <Constant symbol="deg_tailless" value="0.000"/>
            <Constant symbol="deg_eve_rep_cons" value="1"/>
            <Constant symbol="deg_eve_rep" value="0.03"/>
            <DiffEqn symbol-ref="REP_EVE_REP_CONS">
                <Expression>p_rep_eve_rep_cons-deg_rep_eve_rep_cons*REP_EVE_REP_CONS</Expression>
            </DiffEqn>
            <Constant symbol="deg_rep_eve_rep_cons" value="0.001"/>
            <DiffEqn symbol-ref="EVE">
                <Expression>p_eve-deg_eve*EVE</Expression>
            </DiffEqn>
            <Constant symbol="deg_eve" value="0"/>
            <!--    <Disabled>
        <Rule symbol-ref="BICOID">
            <Expression>0.0</Expression>
        </Rule>
    </Disabled>
-->
        </System>
        <Field symbol="BICOID" name="Morphogen_Bicoid" value="0.0294*((l.x/10)^2)-4.789*(l.x/10)+191.88">
            <Diffusion rate="0.16"/>
        </Field>
        <Field symbol="M_NANOS" name="Morphogen_Nanos" value="0.031*((l.x/10)^2)-1.81*(l.x/10)+16.92">
            <Diffusion rate="0.16"/>
        </Field>
        <Field symbol="HUNCHBACK" name="Morphogen_Hunchback" value="-1.189e-05*(l.x/10)^4 +0.002885 * (l.x/10)^3 -0.2223 *(l.x/10)^2 + 4.661 * (l.x/10) + 61">
            <Diffusion rate="2.16"/>
        </Field>
        <Field symbol="CAUDAL" name="Morphogen_Caudal" value="-0.0008175 *(l.x/10)^3 + 0.09382 *(l.x/10)^2 -0.5582 *(l.x/10) + 1">
            <Diffusion rate="2.25"/>
        </Field>
        <Field symbol="KRUPPEL" name="Morphogen" value="0">
            <Diffusion rate="0.67"/>
        </Field>
        <Field symbol="GIANT" name="Morphogen" value="0">
            <Diffusion rate="0.4"/>
        </Field>
        <Field symbol="KNIRPS" name="Morphogen" value="0">
            <Diffusion rate="0.9"/>
        </Field>
        <Field symbol="TAILLESS" name="Morphogen" value="37.81*exp(-1*(((l.x/10)-15)/4.48)^2)+143.2*exp(-1*(((l.x/10)-93.72)/8.137)^2)">
            <Diffusion rate="0"/>
        </Field>
        <Field symbol="EVE_REP" value="0">
            <Diffusion rate="5"/>
        </Field>
        <Field symbol="EVE_REP_CONS" value="0">
            <Diffusion rate="0.0"/>
        </Field>
        <Field symbol="REP_EVE_REP_CONS" value="0">
            <Diffusion rate="4"/>
        </Field>
        <Field symbol="EVE" value="0">
            <Diffusion rate="0"/>
        </Field>
    </Global>
    <Space>
        <Lattice class="square">
            <Size symbol="size" value="1000, 1000, 0"/>
            <BoundaryConditions>
                <Condition type="noflux" boundary="x"/>
                <Condition type="noflux" boundary="y"/>
                <Condition type="noflux" boundary="-x"/>
                <Condition type="noflux" boundary="-y"/>
            </BoundaryConditions>
            <Neighborhood>
                <!--    <Disabled>
        <Order>2</Order>
    </Disabled>
-->
                <Distance>2.5</Distance>
            </Neighborhood>
            <Domain boundary-type="noflux">
                <Image path="M6694_model_domain.tiff"/>
            </Domain>
        </Lattice>
        <SpaceSymbol symbol="l"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="500"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="medium" name="medium">
            <Property symbol="clone" value="0.0"/>
            <Variable symbol="p_bicoid" value="0"/>
            <Variable symbol="p_caudal" value="0"/>
            <Variable symbol="p_giant" value="0"/>
            <Variable symbol="p_kruppel" value="0"/>
            <Variable symbol="p_nanos" value="0"/>
            <Variable symbol="p_hunchback" value="0"/>
            <Variable symbol="p_knirps" value="0"/>
            <Variable symbol="p_tailless" value="0"/>
            <Property symbol="p_eve_rep_cons" value="0"/>
            <Property symbol="p_eve_rep" value="0"/>
            <Property symbol="p_rep_eve_rep_cons" value="0"/>
            <Property symbol="p_eve" value="0"/>
        </CellType>
        <CellType class="biological" name="White_cells_true">
            <VolumeConstraint target="300" strength="1"/>
            <SurfaceConstraint target="1" strength="1" mode="aspherity"/>
            <Property symbol="clone" value="1.0"/>
            <Property symbol="p_bicoid" value="0"/>
            <Property symbol="p_nanos" value="0"/>
            <Property symbol="p_hunchback" value="0"/>
            <Property symbol="p_caudal" value="0"/>
            <Property symbol="p_kruppel" value="0"/>
            <Property symbol="p_giant" value="0"/>
            <Property symbol="p_knirps" value="0"/>
            <Property symbol="p_tailless" value="0"/>
            <Property symbol="p_eve_rep_cons" value="1"/>
            <Property symbol="p_eve_rep" value="0"/>
            <Property symbol="p_rep_eve_rep_cons" value="0"/>
            <Property symbol="p_eve" value="0"/>
            <Mapper name="Map_Bicoid">
                <Input value="BICOID"/>
                <Output symbol-ref="con_bicoid" mapping="average"/>
            </Mapper>
            <Property symbol="con_bicoid" value="0"/>
            <Mapper name="Map_Nanos">
                <Input value="M_NANOS"/>
                <Output symbol-ref="con_nanos" mapping="average"/>
            </Mapper>
            <Property symbol="con_nanos" value="0"/>
            <Mapper name="Map_Caudal">
                <Input value="CAUDAL"/>
                <Output symbol-ref="con_caudal" mapping="average"/>
            </Mapper>
            <Property symbol="con_caudal" value="0"/>
            <Mapper name="Map_Hunchback">
                <Input value="HUNCHBACK"/>
                <Output symbol-ref="con_hunchback" mapping="average"/>
            </Mapper>
            <Property symbol="con_hunchback" value="0"/>
            <Mapper name="Map_Kruppel">
                <Input value="KRUPPEL"/>
                <Output symbol-ref="con_kruppel" mapping="average"/>
            </Mapper>
            <Property symbol="con_kruppel" value="0"/>
            <Mapper name="Map_Giant">
                <Input value="GIANT"/>
                <Output symbol-ref="con_giant" mapping="average"/>
            </Mapper>
            <Property symbol="con_giant" value="0"/>
            <Mapper name="Map_Knirps">
                <Input value="KNIRPS"/>
                <Output symbol-ref="con_knirps" mapping="average"/>
            </Mapper>
            <Property symbol="con_knirps" value="0"/>
            <Mapper name="Map_Tailless">
                <Input value="TAILLESS"/>
                <Output symbol-ref="con_tailless" mapping="average"/>
            </Mapper>
            <Property symbol="con_tailless" value="0"/>
            <Mapper name="Map_Eve_Rep">
                <Input value="EVE_REP"/>
                <Output symbol-ref="con_eve_rep" mapping="average"/>
            </Mapper>
            <Property symbol="con_eve_rep" value="0"/>
            <Mapper name="Map_Eve_Rep_Cons">
                <Input value="EVE_REP_CONS"/>
                <Output symbol-ref="con_eve_rep_cons" mapping="average"/>
            </Mapper>
            <Property symbol="con_eve_rep_cons" value="0"/>
            <Mapper name="Map_Rep_Eve_Rep_Cons">
                <Input value="REP_EVE_REP_CONS"/>
                <Output symbol-ref="con_rep_eve_rep_cons" mapping="average"/>
            </Mapper>
            <Property symbol="con_rep_eve_rep_cons" value="0"/>
            <Mapper name="Map_Rep_Eve_Rep_Cons">
                <Input value="EVE"/>
                <Output symbol-ref="con_eve" mapping="average"/>
            </Mapper>
            <Property symbol="con_eve" value="0"/>
            <System time-step="1.0" name="Morphogenes" solver="Euler [fixed, O(1)]">
                <Rule symbol-ref="p_hunchback">
                    <Expression>if((con_bicoid>19 or con_hunchback>49 or con_tailless>101) and con_caudal&lt;122 and con_kruppel&lt;139 and con_knirps&lt;4 and con_nanos&lt;110, 31.3, 0) 
</Expression>
                </Rule>
                <Rule symbol-ref="p_caudal">
                    <Expression>if((con_nanos>125) and con_hunchback&lt;61, 7.65, 0)</Expression>
                </Rule>
                <Rule symbol-ref="p_kruppel">
                    <Expression>if((con_bicoid>9 or con_kruppel>103 or con_caudal>151) and con_hunchback&lt;176.98 and con_giant&lt;4 and con_knirps&lt;115 and con_tailless&lt;20 and time>4, 16.3, 0)</Expression>
                </Rule>
                <Rule symbol-ref="p_giant">
                    <Expression>if((con_bicoid>40 or con_caudal>133 or con_giant>85) and con_kruppel&lt;15 and con_tailless&lt;102 and time>4, 16.1, 0)</Expression>
                </Rule>
                <Rule symbol-ref="p_knirps">
                    <Expression>if((con_bicoid>5 or con_caudal>152 or con_knirps>94) and con_hunchback&lt;7 and con_kruppel&lt;141 and con_giant&lt;220 and con_tailless&lt;18 and time>4, 17, 0)</Expression>
                </Rule>
                <Rule symbol-ref="p_eve_rep">
                    <Expression>if(con_hunchback>40 and con_giant&lt;30 and con_kruppel&lt;10 and time>40, 50, 
if(con_eve>14, 50, 0)
)</Expression>
                </Rule>
                <Rule symbol-ref="p_rep_eve_rep_cons">
                    <Expression>if(con_hunchback>40 and con_giant&lt;30 and con_kruppel&lt;10 and time>40, 50, 
if(con_eve>14, 50, 0)
)</Expression>
                </Rule>
                <Rule symbol-ref="p_eve_rep_cons">
                    <Expression>if(con_rep_eve_rep_cons>0.1, 0, 1)</Expression>
                </Rule>
                <Rule symbol-ref="p_eve">
                    <Expression>if(con_hunchback>40 and con_giant&lt;30 and con_kruppel&lt;10 and con_nanos&lt;105 and time>40, 2, 
if(con_eve_rep&lt;0.1 and con_eve_rep_cons&lt;0.1 and con_bicoid&lt;70 and con_nanos&lt;115, 2, 0)
)</Expression>
                </Rule>
                <Rule symbol-ref="clone">
                    <Expression> if(con_eve>1, 1, 0)</Expression>
                </Rule>
            </System>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction>
            <!--    <Disabled>
        <Contact type1="amoeba" type2="medium" value="1"/>
    </Disabled>
-->
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <MetropolisKinetics temperature="0.2"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Distance>2.5</Distance>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population type="White_cells_true" size="0">
            <InitRectangle number-of-cells="2800" mode="regular">
                <Dimensions origin="20,250, 0" size="960, 450, 0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="5" decorate="false">
            <Terminal name="png" persist="true"/>
            <Plot>
                <!--    <Disabled>
        <Field symbol-ref="GIANT">
            <ColorMap>
                <Color value="0.0" color="white"/>
                <Color value="1.0" color="blue"/>
            </ColorMap>
        </Field>
    </Disabled>
-->
                <Cells opacity="0.55" value="clone">
                    <ColorMap>
                        <Color value="0.0" color="white"/>
                        <Color value="1.0" color="white"/>
                        <Color value="2.0" color="green"/>
                        <Color value="3.0" color="orange"/>
                        <Color value="4.0" color="light-blue"/>
                        <Color value="5.0" color="gold"/>
                    </ColorMap>
                </Cells>
            </Plot>
        </Gnuplotter>
        <!--    <Disabled>
        <Logger time-step="1">
            <Input>
                <Symbol symbol-ref="HUNCHBACK"/>
                <Symbol symbol-ref="BICOID"/>
                <Symbol symbol-ref="CAUDAL"/>
                <Symbol symbol-ref="NANOS"/>
                <Symbol symbol-ref="TAILLESS"/>
                <Disabled>
                    <Symbol symbol-ref="KRUPPEL"/>
                </Disabled>
                <Disabled>
                    <Symbol symbol-ref="GIANT"/>
                </Disabled>
                <Disabled>
                    <Symbol symbol-ref="KNIRPS"/>
                </Disabled>
            </Input>
            <Output>
                <TextOutput file-name="automatic" file-format="csv" file-numbering="sequential"/>
            </Output>
            <Plots>
                <Plot time-step="5" file-numbering="time">
                    <Style grid="true" point-size="1.0" style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="l.x"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="HUNCHBACK"/>
                        <Symbol symbol-ref="BICOID"/>
                        <Symbol symbol-ref="CAUDAL"/>
                        <Symbol symbol-ref="NANOS"/>
                        <Symbol symbol-ref="TAILLESS"/>
                        <Disabled>
                            <Symbol symbol-ref="KRUPPEL"/>
                        </Disabled>
                        <Disabled>
                            <Symbol symbol-ref="GIANT"/>
                        </Disabled>
                        <Disabled>
                            <Symbol symbol-ref="KNIRPS"/>
                        </Disabled>
                    </Y-axis>
                </Plot>
            </Plots>
        </Logger>
    </Disabled>
-->
        <ModelGraph format="dot" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>
