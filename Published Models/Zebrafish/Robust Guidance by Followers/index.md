---
MorpheusModelID: M0008

title: "Robust Guidance by Followers"

authors: [R. Müller, A. Boutillon, D. Jahn, J. Starruß, N. B. David, L. Brusch]
contributors: [D. Jahn]

# Under review
hidden: false
private: false

# Reference details
publication:
  doi: "10.3389/fams.2023.1163583"
  title: "Collective cell migration due to guidance-by-followers is robust to multiple stimuli"
  journal: "Front. Appl. Math. Stat."
  volume: 9
  #issue: 
  page: "2297-4687"
  year: 2023
  original_model: true
  preprint: false

tags:
#- α-Catenin
- Animal Pole
- Anteroposterior Axis
- Arithmetic Mean
- Axial Cell
- Axial Mesoderm
- Biological Development
- Body Axis
- Cell-autonomous Behavior
- Cell Migration
- Cell Polarity
- CellPopulations
- Cellular Potts Model
- Collective Cell Migration
- Collective Migration
- Collective Phenomenon
- Contact Interface
- Contact Length
- Convergence
- CPM
- DirectedMotion
- Elongating Body Axis
- Embryo
- Embryonic Axis
- Epiboly
- Event
- Extension
- Gastrulation
- Guidance by Followers
- Guidance-by-followers
- Head-on Stimulus
- Individual-based Model
- Internalization
- Long-range Coordination
- Mechanoperception
- Mechanotransduction
- MembraneProperty
- Mesendodermal Polster
- Multiple Neighbors
- NeighborhoodReporter
- Neighbor Information Integration
- Paraxial Mesoderm
- Polster
- Polster Cell
- Population
- Random Movement
- Robustness
- Rule-based Model
- Run and Tumble
- Signal Integration
- Signal Processing
- Space
- Steric Interaction
- Tissue Repair
- Transmission Interface
- Tumor Invasion
- Vectorial Guidance
- Vectorial Guidance Stimuli
- Velocity Vector
- Zebrafish
- Zebrafish Embryo

categories:
- DOI:10.3389/fams.2023.1163583
---
> Guidance-by-followers combined with one of several simple rule sets for integration of signals from multiple neighbors provides a robust mechanism for collective cell migration.

## Introduction

Collective cell migration is an important process during biological development and tissue repair but may turn malignant during tumor invasion. Recently, <a href="/model/m0006/" title="Morpheus Model ID: <code class='model-id-tooltip'>M0006</code>">guidance-by-followers</a> was identified as an underlying mechanism of collective cell migration in the zebrafish embryo. When multiple cells interact simultaneously, this poses the question of how the guidance stimuli are integrated.

In this study, a recent <a href="/model/m0006/" title="Morpheus Model ID: <code class='model-id-tooltip'>M0006</code>">individual-based model</a> is extended by an integration step of the vectorial guidance stimuli and model predictions are compared for different variants of the mechanism:

- **neglecting** guidance over steric interactions
- **arithmetic mean** of stimuli,
- dominance of stimulus with **largest transmission interface**,
- and dominance of **most head-on** stimulus.

## Description

During gastrulation in zebrafish, the elongating body axis is composed of distinct cell populations. The axis is headed by the mesendodermal polster followed by the posterior axial mesoderm. The movement of both these tissues relies on different mechanisms. Individual polster cells undergo active random run-and-tumble migration but, as a group, they coordinate their movement to exhibit guided collective migration. Meanwhile, the posterior axial mesoderm elongates by undergoing convergence and extension.

{{% callout note %}}
**Cell-autonomous behavior** of polster cells (without guidance-by-followers) and the implementation of the local mechanism described as **‘guidance-by-followers’** using <span title="//Space/MembraneLattice & //CellTypes/CellType[@name='cell']/MembraneProperty">`MembraneProperties`</span> and the <span title="//CellTypes/CellType[@name='cell']/DirectedMotion">`DirectedMotion`</span> component is described in model <a href="/model/m0006/" title="Morpheus Model ID: <code class='model-id-tooltip'>M0006</code>">M0006</a> in full detail.
{{% /callout %}}

<div id="fig-1">
![](M0008_robust-guidance-by-followers_fig-01_gbf-during-zebrafish-gastrulation.jpg "<strong>Figure 1:</strong> Collective cell migration due to guidance-by-followers during gastrulation in zebrafish. Schematic of the dorsal **(A)** and lateral **(B)** view of a zebrafish embryo during gastrulation, at 60% epiboly stage. The polster (green), the (posterior) axial mesoderm (yellow), and paraxial mesoderm (gray) are part of the inner layer of cells, and all move toward the animal pole of the embryo. Polster cells undergo a collective migration while axial mesoderm extends by the addition of new cells by internalization and by convergence and extension. These two tissues are made up of several layers of cells and constitute the body axis that elongates during gastrulation. The paraxial mesoderm is a flat monolayer of cells flanking the body axis, which also moves toward the animal pole. *A*, animal pole; *Veg*, vegetal pole; *R*, right; *L*, left, *D*, dorsal; *V*, ventral. **(C, D)** The schematic description of the cell migration model. In **(C)**, the direction of motion is transmitted from a moving cell to a polster cell (green) when it is hit by the moving cell (here, a yellow axial cell). When multiple cells interact simultaneously **(D)**, competing input signals need to be processed to determine the direction of motion of each cell. ([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/): [**Müller _et al._**](#reference), [Fig. 1](https://www.frontiersin.org/articles/10.3389/fams.2023.1163583/full#F1))")
</div>

For tissue-scale simulations, a confined <span title="//Space/Lattice">`Space`</span> in 2D is used, mimicking the presence of paraxial mesoderm on both sides. A <span title="//CellPopulations/Population[@type='cell']">`Population`</span> of $`400`$ cells is initialized and is given $`20\ \text{min}`$ time to settle and adjust their shapes and packing. Once this initial phase is over, two <span title="//CellTypes/CellType[name='cell']/Event[@name='Axial cell initialization' or 'Polster cell initialization']">`Events`</span> are triggered by which the cells are assigned an identity based on their position along the anteroposterior (top-down) axis, and the appropriate motility characteristics are applied to them. Unless stated otherwise, we choose to split the population in a position such that one-third becomes polster and two-thirds become axial cells.

The initial split of the overall cell population into two cell types establishes a front between polster and axial cells. Polster cells (green in [Fig. 1](#fig-1)) exhibit random, run-and-tumble movement and are sensitive to guidance-by-followers. Axial cells (yellow in [Fig. 1](#fig-1)) display a <span title="//CellTypes/CellType[@name='cell']/DirectedMotion">`DirectedMotion`</span> oriented toward the top of the simulation and are not responsive to guidance-by-followers. Both cell types can trigger guidance-by-followers in polster cells.

If there are multiple neighbor cells moving toward the considered cell, then an additional rule for integrating potentially multiple guidance-by-follower signals is needed. To investigate the impact of such signal integration, we here consider and compare four hypothetical signal processing models. In the model implementation, this can be chosen by setting the constant <span title="///Global/Constant[@symbol='pushing_mode']">`pushing_mode`</span> to $`0\ \text{-}\ 3`$. The four signal processing models are defined as follows and are illustrated in [Fig. 2](#fig-2).

- **Model 0** ignores the guidance-by-followers interaction and serves as a quantifiable baseline where cells will collide and squeeze past each other due to **steric interactions**.
- **Model 1** sets the cell velocity to the **arithmetic mean** of neighbors' velocity vectors which fall within a <span title="//Global/Constant[symbol='max_angle']">`max_angle`</span> sector, with the <span title="//CellTypes/CellType[name='cell']/NeighborhoodReporter/Input[@scaling='length']">`length`</span> of each cell-cell contact serving as the weighting factor.
- **Model 2** sets the cell velocity to that of the neighbor which has the **largest contact <span title="//CellTypes/CellType[name='cell']/NeighborhoodReporter/Input[@scaling='length']">`length`</span>** given that its velocity vector falls within the <span title="//Global/Constant[symbol='max_angle']">`max_angle`</span> sector around the contact vector, ignoring other candidates. If the largest contact does not fulfill the <span title="//Global/Constant[symbol='max_angle']">`max_angle`</span> sector criterion because it strikes more tangential, then the next smaller contact is evaluated.
- **Model 3** sets the cell velocity to that of the neighbor with the **best aligned velocity vector**, i.e., the smallest angle between velocity and contact vectors, ignoring other candidates.

<div id="fig-2">
![](M0008_robust-guidance-by-followers_fig-02_cell-cell-interactions.jpg "**Figure 2:** A schematic representation of cell-cell interactions upon contact. **(A)** Three cells, here of different sizes, move as indicated by their velocity vectors (left). Upon contact (right), sectors up to the angle <span title='//Global/Constant[symbol=&apos;max_angle&apos;]'>$`\alpha_\text{max}`$</span> around each velocity vector indicate whether guidance is exerted (when the direction to the cell’s center of mass falls within, yellow) or not (gray). **(B)** Four cell-cell interaction models and their resulting velocity vectors (black unchanged, red changed upon contact): Model 0 is considered as a baseline without velocity changes upon contact. Model 1 yields the mean of the impact velocities, weighted by cell-cell contact length. Model 2 yields the velocity vector of the impact with the largest cell-cell contact length. Model 3 yields the velocity vector of the impact that is oriented closest to the cell center. ([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/): [**Müller _et al._**](#reference), [Fig. 2](https://www.frontiersin.org/articles/10.3389/fams.2023.1163583/full#F2))")
</div>

## Results

The different rule-based models with specific modalities of neighbor information integration by polster cells can quasi-quantitatively predict polster behavior ([Müller _et al._, 2023](#reference)) and produce robust guidance of polster cells by the axial mesoderm (see [Vid. 1](#vid-1) and [Fig. 3](#fig-3) below), suggesting that the guidance-by-followers process is robust to variation in cellular mechanisms such as:

- multiple **conflicting stimuli**,
- variability in **cell size**,
- random cell **motility parameters**,
- variations in **cell numbers**.

For these results and comparison to experimental data please see the [publication](#reference).

<figure id="vid-1">
  ![](M0008_robust-guidance-by-followers_vid-01_gbf-robust-to-multiple-stimuli.mp4)
  <figcaption>
    <strong>Video 1:</strong> Simulation video of {{< model_quick_access "media/model/m0008/M0008_robust-guidance-by-followers_model.xml" >}} (<a href="https://creativecommons.org/licenses/by/4.0/" target="_blank" rel="noopener noreferrer"><em>CC BY 4.0</em></a>: <a href="#reference"><strong>Müller <em>et al.</em></strong></a>, <a href="https://www.frontiersin.org/articles/10.3389/fams.2023.1163583/full#supplementary-material" target="_blank" rel="noopener noreferrer">Supplementary Video 2</a>)
  </figcaption>
</figure>

<div id="fig-3">
![](M0008_robust-guidance-by-followers_fig-03_gbf-simulation-snapshots.jpg "**Figure 3:** Snapshots of a simulation without the guidance-by-follower mechanism (model 0) and of three different realizations of the guidance-by-follower mechanism (models 1–3) at simulation times $`20, 320, 620\ \mathrm{min}`$. Cells are <span title='//CellTypes/CellType[name=&apos;cell&apos;]/Function[@symbol=&apos;color&apos;]'>`color`</span>-coded based on their identity, source of the velocity vector, and alignment. **Yellow:** Axial cells with motility as indicated by the yellow arrow on the left. Leading axial cells define the front speed. **Light green:** Polster cells guided by follower cells such that their orientation points to the same quadrant as the axial front velocity. **Orange:** Polster cells guided by follower cells away from the quadrant of the axial front velocity. **Dark green:** Polster cells with orientation into the same quadrant as the axial front velocity by chance due to run-and-tumble motion. **Red:** Polster cells with run-and-tumble motion away from the quadrant of the axial front velocity. ([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/): [**Müller _et al._**](#reference), [Fig. 3](https://www.frontiersin.org/articles/10.3389/fams.2023.1163583/full#F3))")
</div>
